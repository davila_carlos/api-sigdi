# Sistema disciplinario

Requisitos 😎:
* Tener node.js v14.17.0 👽
* Contar con Postgres como SGBD 🐘

Instrucciones 👨‍💻:

* Ejecutar el siguiente comando: ``git clone  https://gitlab.com/davila_carlos/api-sigdi.git``
* Ejecutar: ``npm install``
* Crear un archivo llamado _.env_ en la raiz del proyecto
* Crear base de datos __dbsigdi__
* Restaurar el _backup_
* Copiar el contenido de _.env.example_ y editar los datos
* Ejecutar el comando ``npm run dev``

Nota 👀:
* El archivo _.env.example_ solo es de ejemplo
* El archivo _.env_ debe ser creado obligatoriamte
* _.env_ indica claramente que datos son requeridos 🙊
