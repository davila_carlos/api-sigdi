import Sequelize from 'sequelize'
require('dotenv').config()

export const sequelize = new Sequelize(
/*   'dbsigdi',
  'backend',
  'b4ck3ndP4ss', */
  'dbsigdi',
  process.env.DB_USERNAME,
  process.env.DB_PASS,
  {
    host: process.env.DB_IP || 'localhost', //192.168.10.98', //192.168.10.98
    port: '5432', //por defecto poner el puerto 5432, esta con el 5433 porque tengo otra versión instalada con el puerto 5432
    dialect: 'postgres',
    pool: {
      max: 5,
      min: 0,
      require: 30000,
      idle: 10000,
    },
    logging: false,
  }
)
