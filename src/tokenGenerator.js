require('dotenv').config()
const jwt = require('jsonwebtoken')

// El token tendra una duración de 48hrs = 172800seg
function generateToken(id, time = 172800) {
  return jwt.sign({ id }, process.env.TOKEN_SECRET, {
    expiresIn: time,
  })
}

function validateToken(token) {
  try {
    return jwt.verify(token, process.env.TOKEN_SECRET)
  } catch (err) {
    console.log(err)
    return null
  }
}

module.exports = {
  generateToken: generateToken,
  validateToken: validateToken,
}
