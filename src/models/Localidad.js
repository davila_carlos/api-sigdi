import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import Departamento from '../models/Departamento'

//Definición de modelo de datos

const Localidad = sequelize.define(
  'localidad',
  {
    idlocalidad: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    iddepartamento: {
      type: Sequelize.INTEGER,
    },
    localidad: {
      type: Sequelize.TEXT,
    },
    idprovincia: {
      type: Sequelize.INTEGER,
    },
    idmunicipio: {
      type: Sequelize.INTEGER,
    },
    departamento: {
      type: Sequelize.TEXT,
    },
    provincia: {
      type: Sequelize.TEXT,
    },
    municipio: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'localidad',
  }
)

Departamento.hasMany(Localidad, { foreignKey: 'iddepartamento' })
//Localidad.belongsTo(Departamento,{foreignKey: 'iddepartamento'})

export default Localidad
