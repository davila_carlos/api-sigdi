import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import TipoPrueba from './TipoPrueba'
import Expediente from './Expediente'

//Definición de modelo de datos

const Prueba = sequelize.define(
  'prueba',
  {
    idprueba: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idexpediente: {
      type: Sequelize.INTEGER,
    },
    idtipoprueba: {
      type: Sequelize.INTEGER,
    },
    ubicacionfisicadocumento: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
    },
    numerofojas: {
      type: Sequelize.INTEGER,
    },
    aceptada: {
      type: Sequelize.BOOLEAN,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'prueba',
  }
)

Expediente.hasMany(Prueba, { foreignKey: 'idexpediente' })
Prueba.belongsTo(Expediente, { foreignKey: 'idexpediente' })

TipoPrueba.hasMany(Prueba, { foreignKey: 'idtipoprueba' })
Prueba.belongsTo(TipoPrueba, { foreignKey: 'idtipoprueba' })

/*Prueba.associate = function(models) {
    Prueba.belongsTo(models.TipoPrueba, { foreignKey: 'idtipoprueba' });
    //Prueba.hasMany(models.Prueba); 
};
Prueba.associate = function(models) {
    Prueba.belongsTo(models.Expediente, { foreignKey: 'idexpediente' });
    //Prueba.hasMany(models.Prueba); 
};*/

/*
Expediente.hasMany(Prueba, { foreingKey: 'idexpediente', sourceKey: 'idexpediente' });
Prueba.belongsTo(Expediente, { foreingKey: 'idexpediente', sourceKey: 'idexpediente' });

TipoPrueba.hasMany(Prueba, { foreingKey: 'idtipoprueba', sourceKey: 'idtipoprueba' });
Prueba.belongsTo(TipoPrueba, { foreingKey: 'idtipoprueba', sourceKey: 'idtipoprueba' });
*/

export default Prueba
