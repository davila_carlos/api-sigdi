'use strict'

import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const TipoFalta = sequelize.define(
  'tipofalta',
  {
    idtipofalta: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipofalta',
  }
)

export default TipoFalta
