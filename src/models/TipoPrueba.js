import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const TipoPrueba = sequelize.define(
  'tipoprueba',
  {
    idtipoprueba: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipoprueba',
  }
)

export default TipoPrueba
