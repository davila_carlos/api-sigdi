import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const Cargo = sequelize.define(
  'cargo',
  {
    idcargo: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'cargo',
  }
)

export default Cargo
