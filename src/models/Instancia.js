import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const Instancia = sequelize.define(
  'instancia',
  {
    idinstancia: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'instancia',
  }
)

export default Instancia
