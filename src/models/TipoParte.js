import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const TipoParte = sequelize.define(
  'tipoparte',
  {
    idtipoparte: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipoparte',
  }
)

export default TipoParte
