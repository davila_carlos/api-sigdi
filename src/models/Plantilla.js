import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const Plantilla = sequelize.define(
  'plantilla',
  {
    idplantilla: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    archivoactuado: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'plantilla',
  }
)

export default Plantilla
