import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import Expediente from './Expediente'

//Definición de modelo de datos

const Sorteo = sequelize.define(
  'sorteo',
  {
    idsorteo: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idexpediente: {
      type: Sequelize.INTEGER,
    },
    idjuzgado: {
      type: Sequelize.INTEGER,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'sorteo',
  }
)

Sorteo.associate = function (models) {
  Sorteo.belongsTo(models.Expediente, { foreignKey: 'idexpediente' })
  //Sorteo.hasMany(models.Sorteo);
}

/*
Expediente.hasMany(Sorteo, { foreingKey: 'idexpediente', sourceKey: 'idexpediente' });
Sorteo.belongsTo(Expediente, { foreingKey: 'idexpediente', sourceKey: 'idexpediente' });
*/

export default Sorteo
