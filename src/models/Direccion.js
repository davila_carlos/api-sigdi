import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

import TipoDireccion from '../models/TipoDireccion'
import Persona from '../models/Persona'

const Direccion = sequelize.define(
  'direccion',
  {
    iddireccion: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idpersona: {
      type: Sequelize.INTEGER,
    },
    idtipodireccion: {
      type: Sequelize.INTEGER,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'direccion',
  }
)

/*Direccion.associate = function (models) {
    Direccion.belongsTo(models.Persona, { foreignKey: 'idpersona'});    
};

Direccion.associate = function (models) {
    Direccion.belongsTo(models.TipoDireccion, { foreignKey: 'idtipodireccion'});    
};*/

Persona.hasMany(Direccion, { foreignKey: 'idpersona' })
Direccion.belongsTo(Persona, { foreignKey: 'idpersona' })

TipoDireccion.hasMany(Direccion, { foreignKey: 'idtipodireccion' })
Direccion.belongsTo(TipoDireccion, { foreignKey: 'idtipodireccion' })

/*Direccion.hasMany(TipoDireccion,{ foreingKey: 'idtipodireccion', sourceKey: 'idtipodireccion' });
TipoDireccion.belongsTo(Direccion, { foreingKey: 'idtipodireccion', sourceKey: 'idtipodireccion'});

Direccion.hasMany(Persona,{ foreingKey: 'idpersona', sourceKey: 'idpersona' });
Persona.belongsTo(Direccion, { foreingKey: 'idpersona', sourceKey: 'idpersona' });*/

export default Direccion
