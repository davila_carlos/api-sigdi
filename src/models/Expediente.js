import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import Unidad from './Unidad'
import TipoMedidaPrecautoria from './TipoMedidaPrecautoria'

//Definición de modelo de datos

const Expediente = sequelize.define(
  'expediente',
  {
    idexpediente: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    cude: {
      type: Sequelize.INTEGER,
    },
    idjuzgado: {
      type: Sequelize.INTEGER,
    },
    relatohechos: {
      type: Sequelize.TEXT,
    },
    fechahoraingresodenuncia: {
      type: Sequelize.DATE,
    },
    numerofojasdenuncia: {
      type: Sequelize.INTEGER,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
    },
    idtipomedidaprecautoria: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'expediente',
  }
)

/*Expediente.associate = function(models) {
    Expediente.belongsTo(models.Unidad, { foreignKey: 'idunidad' });
};*/

Unidad.hasMany(Expediente, { foreignKey: 'idjuzgado' })
Expediente.belongsTo(Unidad, { foreignKey: 'idjuzgado' })

TipoMedidaPrecautoria.hasMany(Expediente, {
  foreignKey: 'idtipomedidaprecautoria',
})
Expediente.belongsTo(TipoMedidaPrecautoria, {
  foreignKey: 'idtipomedidaprecautoria',
})

//Unidad.hasMany(Expediente, { foreingKey: 'idunidad', sourceKey: 'idjuzgado' });
//Expediente.belongsTo(Unidad, { foreingKey: 'idunidad', sourceKey: 'idjuzgado' });

export default Expediente
