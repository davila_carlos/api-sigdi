import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

//Definición de modelo de datos

const Departamento = sequelize.define(
  'departamento',
  {
    iddepartamento: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'departamento',
  }
)

export default Departamento
