import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import Expediente from './Expediente'
import Plantilla from './Plantilla'
import Instancia from './Instancia'
import TipoActuado from './TipoActuado'
import Estado from './Estado'

//Definición de modelo de datos

const Actuado = sequelize.define(
  'actuado',
  {
    idactuado: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idexpediente: {
      type: Sequelize.INTEGER,
    },
    idinstancia: {
      type: Sequelize.INTEGER,
    },
    idtipoactuado: {
      type: Sequelize.INTEGER,
    },
    idestado: {
      type: Sequelize.INTEGER,
    },
    archivoactuado: {
      type: Sequelize.TEXT,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
    },
    fechahorainicio: {
      type: Sequelize.DATE,
    },
    fechahorafin: {
      type: Sequelize.DATE,
    },
    observacion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'actuado',
  }
)

Actuado.associate = function (models) {
  Actuado.belongsTo(models.Expediente, { foreignKey: 'idexpediente' })
}

Actuado.associate = function (models) {
  Actuado.belongsTo(models.Plantilla, { foreignKey: 'idplantilla' })
}
Actuado.associate = function (models) {
  Actuado.belongsTo(models.Instancia, { foreignKey: 'idinstancia' })
}
Actuado.associate = function (models) {
  Actuado.belongsTo(models.TipoActuado, {
    foreignKey: 'idtipoactuado',
  })
}
Actuado.associate = function (models) {
  Actuado.belongsTo(models.Estado, { foreignKey: 'idestado' })
}

/*
Expediente.hasMany(Actuado, { foreingKey: 'idexpediente', sourceKey: 'idexpediente' });
Actuado.belongsTo(Expediente, { foreingKey: 'idexpediente', sourceKey: 'idexpediente' });

Plantilla.hasMany(Actuado, { foreingKey: 'idplantilla', sourceKey: 'idplantilla' });
Actuado.belongsTo(Plantilla, { foreingKey: 'idplantilla', sourceKey: 'idplantilla' });

Instancia.hasMany(Actuado, { foreingKey: 'idinstancia', sourceKey: 'idinstancia' });
Actuado.belongsTo(Instancia, { foreingKey: 'idinstancia', sourceKey: 'idinstancia' });

TipoActuado.hasMany(Actuado, { foreingKey: 'idtipoactuado', sourceKey: 'idtipoactuado' });
Actuado.belongsTo(TipoActuado, { foreingKey: 'idtipoactuado', sourceKey: 'idtipoactuado' });

Estado.hasMany(Actuado, { foreingKey: 'idestado', sourceKey: 'idestado' });
Actuado.belongsTo(Estado, { foreingKey: 'idestado', sourceKey: 'idestado' });

*/

export default Actuado
