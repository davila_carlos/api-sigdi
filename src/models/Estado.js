import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const Estado = sequelize.define(
  'estado',
  {
    idestado: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'estado',
  }
)

export default Estado
