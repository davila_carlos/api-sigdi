import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const TipoDireccion = sequelize.define(
  'tipodireccion',
  {
    idtipodireccion: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipodireccion',
  }
)

export default TipoDireccion
