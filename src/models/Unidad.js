import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import Institucion from '../models/Institucion'
import Departamento from '../models/Departamento'
import Localidad from '../models/Localidad'
import TipoUnidad from '../models/TipoUnidad'

//Definición de modelo de datos

const Unidad = sequelize.define(
  'unidad',
  {
    idunidad: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    idinstitucion: {
      type: Sequelize.INTEGER,
    },
    iddepartamento: {
      type: Sequelize.INTEGER,
    },
    idlocalidad: {
      type: Sequelize.INTEGER,
    },
    idtipounidad: {
      type: Sequelize.INTEGER,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'unidad',
  }
)

Institucion.hasMany(Unidad, { foreignKey: 'idinstitucion' })
Unidad.belongsTo(Institucion, { foreignKey: 'idinstitucion' })

Departamento.hasMany(Unidad, { foreignKey: 'iddepartamento' })
Unidad.belongsTo(Departamento, { foreignKey: 'iddepartamento' })

Localidad.hasMany(Unidad, { foreignKey: 'idlocalidad' })
Unidad.belongsTo(Localidad, { foreignKey: 'idlocalidad' })

TipoUnidad.hasMany(Unidad, { foreignKey: 'idtipounidad' })
Unidad.belongsTo(TipoUnidad, { foreignKey: 'idtipounidad' })

export default Unidad
