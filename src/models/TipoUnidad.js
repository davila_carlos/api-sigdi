import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

//Definición de modelo de datos

const TipoUnidad = sequelize.define(
  'tipounidad',
  {
    idtipounidad: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipounidad',
  }
)

export default TipoUnidad
