import Sequelize from 'sequelize'

import { sequelize } from '../database/database'
//import Usuario from './Usuario';

//Definición de modelo de datos

const Rol = sequelize.define(
  'rol',
  {
    idrol: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    fechahoraregistro: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'rol',
  }
)

export default Rol
