import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

import Expediente from './Expediente'
import Unidad from './Unidad'
import TipoParte from './TipoParte'
import Persona from './Persona'
import Cargo from './Cargo'

const Parte = sequelize.define(
  'parte',
  {
    idparte: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idexpediente: {
      type: Sequelize.INTEGER,
    },
    idunidad: {
      type: Sequelize.INTEGER,
    },
    idcargo: {
      type: Sequelize.INTEGER,
    },
    idtipoparte: {
      type: Sequelize.INTEGER,
    },
    idpersona: {
      type: Sequelize.INTEGER,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
      //defaultValue: sequelize.fn('NOW')
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
      //defaultValue: sequelize.fn('NOW')
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'parte',
  }
)

Persona.hasMany(Parte, { foreignKey: 'idpersona' })
Parte.belongsTo(Persona, { foreignKey: 'idpersona' })

TipoParte.hasMany(Parte, { foreignKey: 'idtipoparte' })
Parte.belongsTo(TipoParte, { foreignKey: 'idtipoparte' })

Unidad.hasMany(Parte, { foreignKey: 'idunidad' })
Parte.belongsTo(Unidad, { foreignKey: 'idunidad' })

Expediente.hasMany(Parte, { foreignKey: 'idexpediente' })
Parte.belongsTo(Expediente, { foreignKey: 'idexpediente' })

Cargo.hasMany(Parte, { foreignKey: 'idcargo' })
Parte.belongsTo(Cargo, { foreignKey: 'idcargo' })

/*Parte.associate = function (models) {
    Parte.belongsTo(models.Expediente, { foreignKey: 'idexpediente'});    
};
Parte.associate = function (models) {
    Parte.belongsTo(models.Unidad, { foreignKey: 'idunidad'});    
};
Parte.associate = function (models) {
    Parte.belongsTo(models.TipoDireccion, { foreignKey: 'idtipodireccion'});    
};
Parte.associate = function (models) {
    Parte.belongsTo(models.TipoParte, { foreignKey: 'idtipoparte'});    
};
Parte.associate = function (models) {
    Parte.belongsTo(models.Persona, { foreignKey: 'idpersona'});    
};*/

//Expediente.hasMany(Parte,{ foreingKey: 'idexpediente', sourceKey: 'idexpediente'});
//Parte.belongsTo(Expediente, {foreingKey: 'idexpediente', sourceKey: 'idexpediente'});

//Unidad.hasMany(Parte,{ foreingKey: 'idunidad', sourceKey: 'idunidad'});
//Parte.belongsTo(Unidad, {foreingKey: 'idunidad', sourceKey: 'idunidad'});

//TipoDireccion.hasMany(Parte,{ foreingKey: 'idtipodireccion', sourceKey: 'idtipodireccion'});
//Parte.belongsTo(TipoDireccion, {foreingKey: 'idtipodireccion', sourceKey: 'idtipodireccion'});

//TipoParte.hasMany(Parte,{ foreingKey: 'idtipoparte', sourceKey: 'idtipoparte'});
//Parte.belongsTo(TipoParte, {foreingKey: 'idtipoparte', sourceKey: 'idtipoparte'});

//Persona.hasMany(Parte,{ foreingKey: 'idpersona', sourceKey: 'idpersona'});
//Parte.belongsTo(Persona, {foreingKey: 'idpersona', sourceKey: 'idpersona'});

export default Parte
