import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

//Definición de modelo de datos

const Institucion = sequelize.define(
  'institucion',
  {
    idinstitucion: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'institucion',
  }
)

export default Institucion
