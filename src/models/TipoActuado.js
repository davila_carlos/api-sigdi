import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const TipoActuado = sequelize.define(
  'tipoactuado',
  {
    idtipoactuado: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idplantilla: {
      type: Sequelize.INTEGER,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipoactuado',
  }
)

export default TipoActuado
