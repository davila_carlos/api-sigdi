import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

import Expedido from '../models/Expedido'

const Persona = sequelize.define(
  'persona',
  {
    idpersona: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    nombre: {
      type: Sequelize.TEXT,
    },
    paterno: {
      type: Sequelize.TEXT,
    },
    materno: {
      type: Sequelize.TEXT,
    },
    ci: {
      type: Sequelize.INTEGER,
    },
    complemento: {
      type: Sequelize.TEXT,
    },
    idexpedido: {
      type: Sequelize.INTEGER,
    },
    edad: {
      type: Sequelize.INTEGER,
    },
    telefono: {
      type: Sequelize.TEXT,
    },
    celular: {
      type: Sequelize.INTEGER,
    },
    profesion: {
      type: Sequelize.TEXT,
    },
    ocupacion: {
      type: Sequelize.TEXT,
    },
    genero: {
      type: Sequelize.TEXT,
    },
    fechanacimiento: {
      type: Sequelize.DATE,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'persona',
  }
)

Persona.hasMany(Expedido, {
  foreingKey: 'idexpedido',
  sourceKey: 'idexpedido',
})
Expedido.belongsTo(Persona, {
  foreingKey: 'idexpedido',
  sourceKey: 'idexpedido',
})

export default Persona
