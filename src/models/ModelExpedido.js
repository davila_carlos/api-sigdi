import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const Expedido = sequelize.define(
  'expedido',
  {
    idexpedido: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    lugarexpedido: {
      type: Sequelize.TEXT,
    },
    expedidocorto: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'expedido',
  }
)

export default Expedido
