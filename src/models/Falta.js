import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

import TipoFalta from './TipoFalta'

//Definición de modelo de datos

const Falta = sequelize.define(
  'falta',
  {
    idfalta: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
    idtipofalta: {
      type: Sequelize.INTEGER,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'falta',
  }
)

//Falta.hasMany(TipoFalta,{ foreingKey: 'idtipofalta', sourceKey: 'idtipofalta'});
//TipoFalta.belongsTo(Falta, {foreingKey: 'idtipofalta', sourceKey: 'idtipofalta'});

Falta.associate = function (models) {
  Falta.belongsTo(models.TipoFalta, { foreignKey: 'idtipofalta' })
}

export default Falta
