import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

const TipoMedidaPrecautoria = sequelize.define(
  'tipomedidaprecautoria',
  {
    idtipomedidaprecautoria: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    descripcion: {
      type: Sequelize.TEXT,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'tipomedidaprecautoria',
  }
)

export default TipoMedidaPrecautoria
