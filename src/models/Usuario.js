import Sequelize from 'sequelize'

import { sequelize } from '../database/database'

const Usuario = sequelize.define(
  'usuario',
  {
    idusuario: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    usuario: {
      type: Sequelize.TEXT,
    },
    clave: {
      type: Sequelize.TEXT,
    },
    idunidad: {
      type: Sequelize.INTEGER,
    },
    idrol: {
      type: Sequelize.INTEGER,
    },
    nombre: {
      type: Sequelize.TEXT,
    },
    paterno: {
      type: Sequelize.TEXT,
    },
    materno: {
      type: Sequelize.TEXT,
    },
    ci: {
      type: Sequelize.INTEGER,
    },
    complemento: {
      type: Sequelize.TEXT,
    },
    idexpedido: {
      type: Sequelize.INTEGER,
    },
    fechanacimiento: {
      type: Sequelize.DATE,
    },
    email: {
      type: Sequelize.TEXT,
    },
    direccion: {
      type: Sequelize.TEXT,
    },
    telefono: {
      type: Sequelize.TEXT,
    },
    telefonooficina: {
      type: Sequelize.TEXT,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
      default: false,
    },
  },
  {
    timestamps: false,
    schema: 'public',
    tableName: 'usuario',
  }
)

Usuario.associate = function (models) {
  Usuario.belongsTo(models.Rol, { foreignKey: 'idrol' })
}

export default Usuario
