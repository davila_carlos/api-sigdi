import Sequelize from 'sequelize'
import { sequelize } from '../database/database'

import Parte from '../models/Parte'
import Falta from '../models/Falta'

const FaltaParte = sequelize.define(
  'faltaparte',
  {
    idfaltaparte: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    idparte: {
      type: Sequelize.INTEGER,
    },
    idfalta: {
      type: Sequelize.INTEGER,
    },
    descripciondenuncia: {
      type: Sequelize.TEXT,
    },
    fechahoraregistro: {
      type: Sequelize.DATE,
      //defaultValue: sequelize.fn('NOW')
    },
    estaactivo: {
      type: Sequelize.BOOLEAN,
    },
    fechahoramodificado: {
      type: Sequelize.DATE,
      //defaultValue: sequelize.fn('NOW')
    },
  },
  {
    timestamps: false,
    //schema: "public",
    tableName: 'faltaparte',
  }
)

Parte.hasMany(FaltaParte, { foreignKey: 'idparte' })
FaltaParte.belongsTo(Parte, { foreignKey: 'idparte' })

Falta.hasMany(FaltaParte, { foreignKey: 'idfalta' })
FaltaParte.belongsTo(Falta, { foreignKey: 'idfalta' })

/*FaltaParte.associate = function (models) {
    FaltaParte.belongsTo(models.Parte, { foreignKey: 'idparte'});    
};

FaltaParte.associate = function (models) {
    FaltaParte.belongsTo(models.Falta, { foreignKey: 'idfalta'});    
};*/

/*Parte.hasMany(FaltaParte,{ foreingKey: 'idparte', sourceKey: 'idparte'});
FaltaParte.belongsTo(Parte, {foreingKey: 'idparte', sourceKey: 'idparte'});


Falta.hasMany(FaltaParte,{ foreingKey: 'idfalta', sourceKey: 'idfalta'});
FaltaParte.belongsTo(Falta, {foreingKey: 'idfalta', sourceKey: 'idfalta'});*/

export default FaltaParte
