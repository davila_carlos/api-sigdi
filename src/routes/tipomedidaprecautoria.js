import { Router } from 'express'

const router = Router()

import { obtenerTipoMedidaPrecautorias } from '../controllers/tipomedidaprecautoria.controller'

router.get('/', obtenerTipoMedidaPrecautorias)

export default router
