'use strict'

import { Router } from 'express'

const router = Router()

import {
  nuevoTipoFalta,
  obtenerTipoFalta,
  obtenerTiposFaltas,
  actualizarTipoFalta,
  eliminaTipoFalta,
  habilitarTipoFalta,
} from '../controllers/tipofalta.controller'

router.post('/', nuevoTipoFalta)
router.get('/:idtipofalta', obtenerTipoFalta)
router.get('/', obtenerTiposFaltas)
router.put('/:idtipofalta', actualizarTipoFalta)
router.delete('/:idtipofalta', eliminaTipoFalta)
router.put('/habilitar/:idtipofalta', habilitarTipoFalta)

export default router
