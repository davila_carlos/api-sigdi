import { Router } from 'express'

import { uploadPrueba } from '../configfiles/datosExpediente'

const router = Router()

import {
  nuevaPrueba,
  obtenerPrueba,
  obtenerPruebas,
  eliminarPrueba,
  actualizarPrueba,
  obtenerPruebasExpediente,
  obtenerArchivoPrueba,
} from '../controllers/prueba.controller'

// /api/rolExpediente
//router.post('/', nuevaPrueba);
router.post(
  '/',
  uploadPrueba.single('ubicacionfisicadocumento'),
  nuevaPrueba
)
router.get(
  '/archivoprueba/:ubicacionfisicadocumento',
  obtenerArchivoPrueba
)
router.get('/', obtenerPruebas)
router.get('/:idprueba', obtenerPrueba)
router.get(
  '/pruebasexpediente/:idexpediente',
  obtenerPruebasExpediente
)
router.delete('/:idprueba', eliminarPrueba)
router.put('/:idprueba', actualizarPrueba)

export default router
