import { Router } from 'express'

const router = Router()

import {
  nuevaPlantilla,
  obtenerPlantilla,
  obtenerPlantillas,
  eliminarPlantilla,
  actualizarPlantilla,
} from '../controllers/plantilla.controller'

// /api/rolExpediente
router.post('/', nuevaPlantilla)
router.get('/', obtenerPlantillas)
router.get('/:idplantilla', obtenerPlantilla)
router.delete('/:idplantilla', eliminarPlantilla)
router.put('/:idplantilla', actualizarPlantilla)

export default router
