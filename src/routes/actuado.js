import { Router } from 'express'

const router = Router()

import {
  nuevoActuado,
  obtenerActuado,
  obtenerActuados,
  eliminarActuado,
  actualizarActuado,
} from '../controllers/actuado.controller'

// /api/rolExpediente
router.post('/', nuevoActuado)
router.get('/', obtenerActuados)
router.get('/:idactuado', obtenerActuado)
router.delete('/:idactuado', eliminarActuado)
router.put('/:idactuado', actualizarActuado)

export default router
