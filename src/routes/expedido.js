import { Router } from 'express'

const router = Router()

import {
  obtenerExpedidos,
  obtenerExpedido,
} from '../controllers/expedido.controller'

router.get('/', obtenerExpedidos)
router.get('/:idexpedido', obtenerExpedido)

console.log(__dirname)
export default router
