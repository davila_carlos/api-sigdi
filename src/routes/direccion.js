import { Router } from 'express'

const router = Router()

import {
  nuevaDireccion,
  obtenerDireccionesPersona,
} from '../controllers/direccion.controller'

router.post('/', nuevaDireccion)
router.get('/persona/:idpersona', obtenerDireccionesPersona)

export default router
