import { Router } from 'express'

const router = Router()

import { obtenerCargos } from '../controllers/cargo.controller'

router.get('/', obtenerCargos)

export default router
