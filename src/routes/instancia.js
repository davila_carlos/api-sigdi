import { Router } from 'express'

const router = Router()

import {
  nuevaInstancia,
  obtenerInstancia,
  obtenerInstancias,
  eliminarInstancia,
  actualizarInstancia,
} from '../controllers/instancia.controller'

// /api/rolExpediente
router.post('/', nuevaInstancia)
router.get('/', obtenerInstancias)
router.get('/:idinstancia', obtenerInstancia)
router.delete('/:idinstancia', eliminarInstancia)
router.put('/:idinstancia', actualizarInstancia)

export default router
