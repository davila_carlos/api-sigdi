import { Router } from 'express'

const router = Router()

import {
  nuevoUsuario,
  obtenerUsuario,
  obtenerUsuarios,
  eliminaUsuario,
  actualizarUsuario,
  realizarLogin,
  obtenerLogueo,
  cambiarClavePersona,
  resetearClave,
  confirmToken,
} from '../controllers/usuario.controller'

// route: /api/usuario
router.post('/', nuevoUsuario)
router.get('/', obtenerUsuarios)
router.get('/:idusuario', obtenerUsuario)
router.delete('/:idusuario', eliminaUsuario)
router.put('/:idusuario', actualizarUsuario)

router.post('/login/', realizarLogin)
router.post('/logueo/', obtenerLogueo)

router.post('/cambiarclave/', cambiarClavePersona)
router.post('/resetearclave/', resetearClave)

router.get('/confirm/:token', confirmToken)

export default router
