import { Router } from 'express'

const router = Router()

import {
  nuevoRol,
  obtenerRol,
  obtenerRoles,
  eliminaRol,
  actualizarRol,
} from '../controllers/rol.controller'

// /api/rol

router.post('/', nuevoRol)
router.get('/', obtenerRoles)
router.get('/:idrol', obtenerRol)
router.delete('/:idrol', eliminaRol)
router.put('/:idrol', actualizarRol)

export default router
