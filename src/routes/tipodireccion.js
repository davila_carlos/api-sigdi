import { Router } from 'express'

const router = Router()

import {
  nuevoTipoDireccion,
  obtenerTipoDireccion,
  obtenerTiposDirecciones,
  actualizarTipoDireccion,
  eliminaTipoDireccion,
  habilitarTipoDireccion,
} from '../controllers/tipodireccion.controller'

router.post('/', nuevoTipoDireccion)
router.get('/:idtipodireccion', obtenerTipoDireccion)
router.get('/', obtenerTiposDirecciones)
router.put('/:idtipodireccion', actualizarTipoDireccion)
router.delete('/:idtipodireccion', eliminaTipoDireccion)
router.put('/habilitar/:idtipodireccion', habilitarTipoDireccion)

export default router
