import { Router } from 'express'

const router = Router()

import {
  nuevoSorteo,
  obtenerSorteo,
  obtenerSorteos,
  eliminarSorteo,
  actualizarSorteo,
} from '../controllers/sorteo.controller'

// /api/rolExpediente
router.post('/', nuevoSorteo)
router.get('/', obtenerSorteos)
router.get('/:idsorteo', obtenerSorteo)
router.delete('/:idsorteo', eliminarSorteo)
router.put('/:idsorteo', actualizarSorteo)

export default router
