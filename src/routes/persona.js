import { Router } from 'express'
import { habilitarEstado } from '../controllers/estado.controller'

const router = Router()

import {
  nuevaPersona,
  obtenerPersona,
  obtenerPersonas,
  actualizarPersona,
  eliminarPersona,
  habilitarPersona,
  obtenerPersonaCI,
} from '../controllers/persona.controller'

router.post('/', nuevaPersona)
router.get('/:idpersona', obtenerPersona)
router.get('/', obtenerPersonas)
router.put('/:idpersona', actualizarPersona)
router.delete('/:idpersona', eliminarPersona)
router.put('/habilitar/:idpersona', habilitarPersona)
router.get('/ci/:ci', obtenerPersonaCI)

export default router
