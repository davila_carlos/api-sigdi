import { Router } from 'express'

const router = Router()

// import { nuevoExpediente, obtenerExpedientes, obtenerExpediente, eliminarExpediente, actualizarExpediente } from '../controllers/expediente.controller';
import {
  nuevoExpediente,
  obtenerExpedientes,
  obtenerExpediente,
  eliminarExpediente,
  actualizarExpediente,
  obtenerExpedientesJuzgado,
} from '../controllers/expediente.controller'

// /api/rolExpediente
router.post('/', nuevoExpediente)
router.get('/', obtenerExpedientes)
router.get('/:idexpediente', obtenerExpediente)
router.get('/juzgado/:idunidad', obtenerExpedientesJuzgado)
router.delete('/:idexpediente', eliminarExpediente)
router.put('/:idexpediente', actualizarExpediente)

export default router
