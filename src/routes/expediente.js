import { Router } from 'express'
import { uploadExpediente } from '../configfiles/datosExpediente'

const router = Router()

import {
  nuevoExpediente,
  obtenerExpedientes,
  obtenerExpediente,
  eliminarExpediente,
  actualizarExpediente,
  obtenerExpedientesJuzgado,
  obtenerArchivoExpediente,
  obtenerExpedientesPersona,
} from '../controllers/expediente.controller'

//router.post('/',subirarchivo,nuevoExpediente);
//router.post('/',subirExpediente.single('relatohechos'),nuevoExpediente);
router.post(
  '/',
  uploadExpediente.single('relatohechos'),
  nuevoExpediente
)
router.get('/archivoexp/:relatohechos', obtenerArchivoExpediente)

router.get('/', obtenerExpedientes)
router.get('/:idexpediente', obtenerExpediente)
router.get('/juzgado/:idunidad', obtenerExpedientesJuzgado)
router.delete('/:idexpediente', eliminarExpediente)
router.put('/:idexpediente', actualizarExpediente)

router.get('/persona/:idpersona', obtenerExpedientesPersona)

export default router
