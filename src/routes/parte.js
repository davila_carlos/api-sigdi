import { Router } from 'express'
import { nuevaFaltaParte } from '../controllers/faltaparte.controller'

const router = Router()

import {
  nuevaParte,
  obtenerParte,
  obtenerPartes,
  obtenerPartesExpediente,
  actualizarParte,
  eliminaParte,
} from '../controllers/parte.controller'

router.post('/', nuevaParte)
router.get('/:idparte', obtenerParte)
router.get('/', obtenerPartes)
router.get('/partesexpediente/:idexpediente', obtenerPartesExpediente)
router.put('/:idparte', actualizarParte)
router.delete('/:idparte', eliminaParte)

export default router
