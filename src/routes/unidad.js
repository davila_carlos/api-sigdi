import { Router } from 'express'

const router = Router()

import {
  nuevaUnidad,
  obtenerUnidad,
  obtenerUnidades,
  eliminarUnidad,
  actualizarUnidad,
} from '../controllers/unidad.controller'

// /api/unidad

router.post('/', nuevaUnidad)
router.get('/', obtenerUnidades)
router.get('/:idunidad', obtenerUnidad)
router.delete('/:idunidad', eliminarUnidad)
router.put('/:idunidad', actualizarUnidad)

export default router
