import { Router } from 'express'

const router = Router()

import {
  nuevoTipoActuado,
  obtenerTipoActuado,
  obtenerTipoActuados,
  eliminarTipoActuado,
  actualizarTipoActuado,
} from '../controllers/tipoactuado.controller'

// /api/rolExpediente
router.post('/', nuevoTipoActuado)
router.get('/', obtenerTipoActuados)
router.get('/:idtipoactuado', obtenerTipoActuado)
router.delete('/:idtipoactuado', eliminarTipoActuado)
router.put('/:idtipoactuado', actualizarTipoActuado)

export default router
