import { Router } from 'express'

const router = Router()

import {
  nuevoEstado,
  obtenerEstado,
  obtenerEstados,
  actualizarEstado,
  eliminaEstado,
  habilitarEstado,
} from '../controllers/estado.controller'

router.post('/', nuevoEstado)
router.get('/:idestado', obtenerEstado)
router.get('/', obtenerEstados)
router.put('/:idestado', actualizarEstado)
router.delete('/:idestado', eliminaEstado)
router.put('/habilitar/:idestado', habilitarEstado)

export default router
