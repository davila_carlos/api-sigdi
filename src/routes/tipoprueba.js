import { Router } from 'express'

const router = Router()

import {
  nuevoTipoPrueba,
  obtenerTipoPrueba,
  obtenerTipoPruebas,
  eliminarTipoPrueba,
  actualizarTipoPrueba,
} from '../controllers/tipoprueba.controller'

// /api/tipoPrueba
router.post('/', nuevoTipoPrueba)
router.get('/', obtenerTipoPruebas)
router.get('/:idtipoprueba', obtenerTipoPrueba)
router.delete('/:idtipoprueba', eliminarTipoPrueba)
router.put('/:idtipoprueba', actualizarTipoPrueba)

export default router
