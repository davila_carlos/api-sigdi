import { Router } from 'express'

const router = Router()

import {
  nuevaFaltaParte,
  obtenerFaltasdelaParte,
} from '../controllers/faltaparte.controller'

router.post('/', nuevaFaltaParte)
router.get('/:idparte', obtenerFaltasdelaParte)

export default router
