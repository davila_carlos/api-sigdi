import { Router } from 'express'

const router = Router()

import {
  obtenerFaltasDeTipoFalta,
  nuevaFalta,
  obtenerFalta,
  obtenerFaltas,
  actualizarFalta,
  eliminaFalta,
  habilitarFalta,
} from '../controllers/falta.controller'

router.post('/', nuevaFalta)
router.get('/:idfalta', obtenerFalta)
router.get('/tipofalta/:idtipofalta', obtenerFaltasDeTipoFalta)
router.get('/', obtenerFaltas)
router.put('/:idfalta', actualizarFalta)
router.delete('/:idfalta', eliminaFalta)
router.put('/habilitar/:idfalta', habilitarFalta)

export default router
