import { Router } from 'express'

import { obtenerTiposDirecciones } from '../controllers/tipodireccion.controller'

const router = Router()

import {
  actualizarTipoParte,
  nuevoTipoParte,
  obtenerTipoParte,
  obtenerTiposPartes,
  eliminaTipoParte,
  habilitarTipoParte,
} from '../controllers/tipoparte.controller'

router.post('/', nuevoTipoParte)
router.get('/:idtipoparte', obtenerTipoParte)
router.get('/', obtenerTiposPartes)
router.put('/:idtipoparte', actualizarTipoParte)
router.delete('/:idtipoparte', eliminaTipoParte)
router.put('/habilitar/:idtipoparte', habilitarTipoParte)

export default router
