'use strict'

import TipoParte from '../models/TipoParte'

export async function obtenerTiposPartes(req, res) {
  //---

  try {
    const tipoparte = await TipoParte.findAll({
      where: {
        estaactivo: true,
      },
    })

    if (tipoparte.length > 0) {
      res.json({
        data: tipoparte,
      })
    } else {
      res.status(500).json({
        message: 'No se tiene registros de tipo parte.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta',
    })
  }
}

//---------------------------------------

export async function nuevoTipoParte(req, res) {
  const { descripcion } = req.body

  try {
    let nuevoTipoParte = await TipoParte.create(
      {
        descripcion,
      },
      {
        fields: ['descripcion'],
      }
    )
    if (nuevoTipoParte)
      return res.json({
        message: 'El Tipo de Parte fue creado',
        data: nuevoTipoParte,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Error, al registar el Tipo Parte',
      data: { e },
    })
  }
}

export async function obtenerTipoParte(req, res) {
  const { idtipoparte } = req.params

  try {
    const tipoParte = await TipoParte.findOne({
      where: {
        idtipoparte,
      },
    })

    if (tipoParte !== null) {
      res.json(tipoParte)
    } else {
      res.status(500).json({
        message: 'Error, tipo de parte no encontrado. ',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function actualizarTipoParte(req, res) {
  const { idtipoparte } = req.params

  const { descripcion } = req.body

  try {
    if (idtipoparte !== null) {
      const tipoparte = await TipoParte.findAll({
        //atributes: ['idtipodireccion', 'descripcion'],
        atributes: ['idtipoparte'],
        where: {
          idtipoparte,
          estaactivo: true,
        },
      })
      if (tipoparte.length > 0) {
        tipoparte.forEach(async tipoparteactualizada => {
          await tipoparteactualizada.update({
            descripcion,
          })
        })
        return res.json({
          message: 'Tipo de Parte actualizado con éxito',
          data: tipoparte,
        })
      } else {
        return res.json({
          message:
            'Tipo Parte No encontrado, no existe el identificador o el estado esta inactivo',
          data: tipoparte,
        })
      }
    } else {
      res.status(500).json({
        message: 'Tipo Parte no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el Tipo Parte',
      data: {},
    })
  }
}

export async function eliminaTipoParte(req, res) {
  const { idtipoparte } = req.params
  try {
    const tipoparte = await TipoParte.findAll({
      atributes: ['idtipoparte'],
      where: {
        idtipoparte,
        estaactivo: true,
      },
    })
    if (tipoparte.length > 0) {
      console.log(tipoparte.estaactivo)
      tipoparte.forEach(async tipoParte => {
        await tipoParte.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Tipo Parte eliminado.',
      })
    } else {
      return res.json({
        message:
          'Tipo Parte No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Tipo Parte.',
      data: {},
    })
  }
}

export async function habilitarTipoParte(req, res) {
  const { idtipoparte } = req.params
  try {
    const tipoparte = await TipoParte.findAll({
      atributes: ['idtipoparte'],
      where: {
        idtipoparte,
        estaactivo: false,
      },
    })
    if (tipoparte.length > 0) {
      console.log(tipoparte.estaactivo)
      tipoparte.forEach(async tipoParte => {
        await tipoParte.update({
          estaactivo: true,
        })
      })
      return res.json({
        message: 'Tipo Parte Habilitado.',
      })
    } else {
      return res.json({
        message:
          'Tipo Parte No encontrado, no existe el identificador o el estado esta activo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede habiliar el Tipo Parte.',
      data: {},
    })
  }
}
