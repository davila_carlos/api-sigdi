'use strict'

import Expedido from '../models/ModelExpedido'

export async function obtenerExpedidos(req, res) {
  try {
    const expedidos = await Expedido.findAll({
      where: {
        estaactivo: true,
      },
      order: [['lugarexpedido', 'ASC']],
    })

    if (expedidos.length > 0) {
      res.json({
        data: expedidos,
      })
    } else {
      res.json({
        data: expedidos,
        message: 'No se tiene registros de expedidos',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      message: 'Sucedió un error, lo sentimos',
      data: { error },
    })
  }
}

//--------------------------------------

export async function obtenerExpedido(req, res) {
  console.log('aqui')
  const { idexpedido } = req.params
  try {
    const expedido_ = await Expedido.findOne({
      where: {
        idexpedido,
      },
    })
    console.log(idexpedido)
    res.json(expedido_)
  } catch (error) {
    console.log(error)
  }
}
