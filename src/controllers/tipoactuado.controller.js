import TipoActuado from '../models/TipoActuado'

export async function nuevoTipoActuado(req, res) {
  const { descripcion } = req.body

  try {
    let nuevoTipoActuado = await TipoActuado.create(
      { descripcion, idplantilla },
      { fields: ['descripcion', 'idplantilla'] }
    )

    if (nuevoTipoActuado)
      return res.json({
        message: 'El Tipo de Actuado fue creado',
        data: nuevoTipoActuado,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message:
        'Hubo un error, no se pudo registar el Tipo de Actuado',
      data: { e },
    })
  }
}

export async function obtenerTipoActuado(req, res) {
  const { idtipoactuado } = req.params
  try {
    const untipoactuado = await TipoActuado.findOne({
      where: {
        idtipoactuado,
        estaactivo: true,
      },
    })

    if (untipoactuado !== null) {
      res.json(untipoactuado)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no se pudo encontrar el tipo de actuado',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerTipoActuados(req, res) {
  try {
    const tipoActuados = await TipoActuado.findAll({
      atributes: ['descripcion', 'idplantilla'],
      where: {
        estaactivo: true,
      },
    })

    if (tipoActuados !== null) {
      res.json(tipoActuados)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no se pudo encontrar el tipo de actuado',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function eliminarTipoActuado(req, res) {
  const { idtipoactuado } = req.params
  try {
    const tipoactuado = await TipoActuado.findAll({
      atributes: ['idtipoactuado'],
      where: {
        idtipoactuado,
        estaactivo: true,
      },
    })
    if (tipoactuado.length > 0) {
      console.log(tipoactuado.estaactivo)
      tipoactuado.forEach(async tipoactuado => {
        await tipoactuado.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'El tipo actuado fue eliminado',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el tipo actuado',
      data: {},
    })
  }
}

export async function actualizarTipoActuado(req, res) {
  const { idtipoactuado } = req.params
  const { descripcion, idplantilla } = req.body
  try {
    if (idtipoactuado !== null) {
      const tipoactuado = await TipoActuado.findAll({
        atributes: ['idtipoactuado'],
        where: {
          idtipoactuado,
          estaactivo: true,
        },
      })
      if (tipoactuado.length > 0) {
        tipoactuado.forEach(async tipoActuadoActualizado => {
          await tipoActuadoActualizado.update({
            descripcion,
            idplantilla,
          })
        })
        return res.json({
          message: 'Tipo Actuado actualizado con éxito',
          data: tipoactuado,
        })
      }
    } else {
      res.status(500).json({ message: 'Tipo Actuado no encontrado' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el Tipo Actuado',
      data: {},
    })
  }
}
