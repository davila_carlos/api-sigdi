'use strict'

import Persona from '../models/Persona'
import { fechaDeHoy } from '../configfiles/datosExpediente'

export async function nuevaPersona(req, res) {
  const {
    nombre,
    paterno,
    materno,
    ci,
    complemento,
    idexpedido,
    edad,
    telefono,
    celular,
    profesion,
    ocupacion,
    genero,
    fechanacimiento,
  } = req.body

  //console.log(req.body);

  try {
    const fechahoraregistro = fechaDeHoy
    const fechahoramodificado = fechaDeHoy

    let nuevaPersona = await Persona.create(
      {
        nombre,
        paterno,
        materno,
        ci,
        complemento,
        idexpedido,
        edad,
        telefono,
        celular,
        profesion,
        ocupacion,
        genero,
        fechanacimiento,
        fechahoraregistro,
        fechahoramodificado,
      },
      {
        fields: [
          'nombre',
          'paterno',
          'materno',
          'ci',
          'complemento',
          'idexpedido',
          'edad',
          'telefono',
          'celular',
          'profesion',
          'ocupacion',
          'genero',
          'fechanacimiento',
          'fechahoraregistro',
          'fechahoramodificado',
        ],
      }
    )
    if (nuevaPersona) {
      return res.json({
        message: 'Se registro correctamente a la persona.',
        data: nuevaPersona,
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'ERROR: No se pudo registrar a la persona.',
      data: { e },
    })
  }
}

export async function obtenerPersona(req, res) {
  const { idpersona } = req.params

  try {
    const persona = await Persona.findOne({
      where: {
        idpersona,
      },
    })

    if (persona !== null) {
      res.json(persona)
    } else {
      res.status(500).json({
        message: 'Hubo un error, no se pudo encontrar la persona',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerPersonas(req, res) {
  try {
    const persona = await Persona.findAll({
      atributes: [
        'idpersona',
        'nombre',
        'paterno',
        'materno',
        'ci',
        'complemento',
        'idexpediente',
        'edad',
        'telefono',
        'celular',
        'profesion',
        'ocupacion',
        'genero',
        'fechanacimiento',
        'estaactivo',
      ],
      where: {
        estaactivo: true,
      },
      order: [['idpersona', 'DESC']],
    })
    res.json({
      data: persona,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function actualizarPersona(req, res) {
  const { idpersona } = req.params

  const {
    nombre,
    paterno,
    materno,
    ci,
    complemento,
    idexpedido,
    edad,
    telefono,
    celular,
    profesion,
    ocupacion,
    genero,
    fechanacimiento,
  } = req.body

  console.log(req.body)

  try {
    if (idpersona !== null) {
      const persona = await Persona.findAll({
        //atributes: ['idtipodireccion', 'descripcion'],
        atributes: ['idpersona'],
        where: {
          idpersona,
          estaactivo: true,
        },
      })
      if (persona.length > 0) {
        persona.forEach(async personaactualizada => {
          await personaactualizada.update({
            nombre,
            paterno,
            materno,
            ci,
            complemento,
            idexpedido,
            edad,
            telefono,
            celular,
            profesion,
            ocupacion,
            genero,
            fechanacimiento,
          })
        })
        return res.json({
          message: 'Persona actualizado con éxito',
          data: persona,
        })
      } else {
        return res.json({
          message:
            'Persona No encontrado, no existe el identificador o el estado esta inactivo ',
          data: persona,
        })
      }
    } else {
      res.status(500).json({
        message: 'Persona no encontrado.',
      })
    }
  } catch (e) {
    console.log(e.message)
    res.json({
      message: 'No se puede actualizar la Persona',
      data: {},
    })
  }
}

export async function eliminarPersona(req, res) {
  const { idpersona } = req.params
  try {
    const persona = await Persona.findAll({
      atributes: ['idpersona'],
      where: {
        idpersona,
        estaactivo: true,
      },
    })
    if (persona.length > 0) {
      console.log(persona.estaactivo)
      persona.forEach(async personactualizada => {
        await personactualizada.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Persona eliminado.',
      })
    } else {
      return res.json({
        message:
          'Persona No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Persona.',
      data: {},
    })
  }
}

export async function habilitarPersona(req, res) {
  const { idpersona } = req.params
  try {
    const persona = await Persona.findAll({
      atributes: ['idpersona'],
      where: {
        idpersona,
        estaactivo: false,
      },
    })
    if (persona.length > 0) {
      console.log(persona.estaactivo)
      persona.forEach(async personaactualizar => {
        await personaactualizar.update({
          estaactivo: true,
        })
      })
      return res.json({
        message: 'Persona Habilitado.',
      })
    } else {
      return res.json({
        message:
          'Persona No encontrado, no existe el identificador o el estado esta activo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede habilitar la Persona.',
      data: {},
    })
  }
}

export async function obtenerPersonaCI(req, res) {
  const { ci } = req.params
  const { complemento } = req.body

  try {
    const persona = await Persona.findOne({
      where: {
        ci,
        complemento,
      },
    })

    if (persona !== null) {
      res.json(persona)
    } else {
      res.status(500).json({
        message: 'Hubo un error, no se pudo encontrar la persona',
      })
    }
  } catch (error) {
    console.log(error)
  }
}
