import TipoPrueba from '../models/TipoPrueba'

export async function obtenerTipoPruebas(req, res) {
  //---

  try {
    const tipoPruebas = await TipoPrueba.findAll({
      atributes: ['descripcion'],
      where: {
        estaactivo: true,
      },
    })

    if (tipoPruebas.length > 0) {
      res.json({
        data: tipoPruebas,
      })
    } else {
      res.status(500).json({
        message: 'No se tiene registros de tipo prueba.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta',
    })
  }
}

//--------------------

export async function nuevoTipoPrueba(req, res) {
  const { descripcion } = req.body

  try {
    let nuevoTipoPrueba = await TipoPrueba.create(
      { descripcion },
      { fields: ['descripcion'] }
    )

    if (nuevoTipoPrueba)
      return res.json({
        message: 'El Tipo de Prueba fue creado',
        data: nuevoTipoPrueba,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el Tipo de Prueba',
      data: { e },
    })
  }
}

export async function obtenerTipoPrueba(req, res) {
  const { idtipoprueba } = req.params
  try {
    const untipoprueba = await TipoPrueba.findOne({
      where: {
        idtipoprueba,
        estaactivo: true,
      },
    })

    if (untipoprueba !== null) {
      res.json(untipoprueba)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no se pudo encontrar el tipo de prueba',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function eliminarTipoPrueba(req, res) {
  const { idtipoprueba } = req.params
  try {
    const tipoprueba = await TipoPrueba.findAll({
      atributes: ['idtipoprueba'],
      where: {
        idtipoprueba,
        estaactivo: true,
      },
    })
    if (tipoprueba.length > 0) {
      console.log(tipoprueba.estaactivo)
      tipoprueba.forEach(async tipoprueba => {
        await tipoprueba.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'La unidad fue eliminada',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar la unidad',
      data: {},
    })
  }
}

export async function actualizarTipoPrueba(req, res) {
  const { idtipoprueba } = req.params
  const { descripcion } = req.body

  try {
    if (idtipoprueba !== null) {
      const tipoprueba = await TipoPrueba.findAll({
        atributes: ['idtipoprueba'],
        where: {
          idtipoprueba,
          estaactivo: true,
        },
      })
      if (tipoprueba.length > 0) {
        tipoprueba.forEach(async tipoPruebaActualizada => {
          await tipoPruebaActualizada.update({
            descripcion,
          })
        })
        return res.json({
          message: 'Unidad actualizada con éxito',
          data: tipoprueba,
        })
      }
    } else {
      res.status(500).json({ message: 'Unidad no encontrada' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar la unidad',
      data: {},
    })
  }
}
