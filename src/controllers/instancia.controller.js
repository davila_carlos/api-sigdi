import Instancia from '../models/Instancia'

export async function nuevaInstancia(req, res) {
  const { descripcion, estaactivo } = req.body
  try {
    let nuevaInstancia = await Instancia.create(
      {
        //idunidad,
        descripcion,
        estaactivo,
      },
      {
        fields: ['descripcion', 'estaactivo'],
      }
    )
    if (nuevaInstancia)
      return res.json({
        message: 'La Instancia fue creada',
        data: nuevaInstancia,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar la instancia',
      data: { e },
    })
  }
}

export async function obtenerInstancia(req, res) {
  const { idinstancia } = req.params
  try {
    const unainstancia = await Instancia.findOne({
      where: {
        idinstancia,
      },
    })
    if (unainstancia !== null) {
      res.json(unainstancia)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener la instancia solicitada',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerInstancias(req, res) {
  try {
    const instancia = await Instancia.findAll({
      atributes: ['idinstancia', 'descripcion', 'estaactivo'],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: instancia,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function eliminarInstancia(req, res) {
  const { idinstancia } = req.params
  try {
    const instancia = await Instancia.findAll({
      atributes: ['idntancia'],
      where: {
        idinstancia,
        estaactivo: true,
      },
    })
    if (instancia.length > 0) {
      console.log(instancia.estaactivo)
      instancia.forEach(async instancia => {
        await instancia.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'La instancia fue eliminada',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar la instancia',
      data: {},
    })
  }
}

export async function actualizarInstancia(req, res) {
  const { idinstancia } = req.params
  const { descripcion } = req.body

  try {
    if (idinstancia !== null) {
      const instancia = await Instancia.findAll({
        atributes: ['idinstancia'],
        where: {
          idinstancia,
          estaactivo: true,
        },
      })
      if (instancia.length > 0) {
        instancia.forEach(async instanciaActualizada => {
          await instanciaActualizada.update({
            descripcion,
          })
        })
        return res.json({
          message: 'Instancia actualizada con éxito',
          data: instancia,
        })
      }
    } else {
      res.status(500).json({ message: 'Instancia no encontrada' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar la instancia',
      data: {},
    })
  }
}
