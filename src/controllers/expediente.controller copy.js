import Expediente from '../models/Expediente'
import Unidad from '../models/Unidad'

export async function nuevoExpediente(req, res) {
  const {
    cude,
    idjuzgado,
    relatohechos,
    fechahoraingresodenuncia,
    numerofojasdenuncia,
  } = req.body
  try {
    //verificar duplicidad
    //const verificarexpediente = await Expediente.findOne({ where: relatohechos, numerofojasdenuncia, estaactivo:true });

    // if (verificarexpediente === null) {

    const existeunidad = await Unidad.findAll({
      where: {
        idunidad: idjuzgado,
        estaactivo: true,
      },
    })
    if (existeunidad.length > 0) {
      let fechahoraregistro = new Date()
      let fechahoramodificado = new Date()
      let nuevoExpediente = await Expediente.create(
        {
          cude,
          idjuzgado,
          relatohechos,
          fechahoraingresodenuncia,
          numerofojasdenuncia,
          fechahoraregistro,
          fechahoramodificado,
        },
        {
          fields: [
            'cude',
            'idjuzgado',
            'relatohechos',
            'fechahoraingresodenuncia',
            'numerofojasdenuncia',
            'fechahoraregistro',
            'fechahoramodificado',
          ],
        }
      )

      if (nuevoExpediente)
        return res.json({
          message: 'El Expediente fue creado',
          data: nuevoExpediente,
        })
    } else {
      return res.json({
        message: 'El juzgado no esta registrado',
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el expediente',
      data: { e },
    })
  }
}

export async function obtenerExpediente(req, res) {
  const { idexpediente } = req.params
  try {
    const unexpediente = await Expediente.findOne({
      where: {
        idexpediente,
      },
    })
    if (unexpediente !== null) {
      res.json(unexpediente)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener el expediente solicitado',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerExpedientesJuzgado(req, res) {
  const { idunidad } = req.params
  console.log(idunidad)
  try {
    if (idunidad !== null) {
      const existeunidad = await Unidad.findAll({
        where: {
          idunidad: idunidad,
          estaactivo: true,
        },
      })
      if (existeunidad.length > 0) {
        /*const expedientes = await Unidad.findAll({                
                    where:{ idunidad:idunidad },                
                    include:[{
                        model: Expediente,
                        where:{ idjuzgado:idunidad },                        
                    }]
                })*/
        const expedientes = await Expediente.findAll({
          where: { idjuzgado: idunidad },
          include: [
            {
              model: Unidad,
              where: { idunidad: idunidad },
            },
          ],
        })
        //res.send(expedientes);
        /*.then((expedientes)=>{
                    res.json({
                        data: expedientes
                    });
                })
                .catch((err)=>{
                    res.status(500).send({
                        message: err.message || "No se encontraron expedientes para esa unidad",
                    })
                }); */

        if (expedientes.length > 0) {
          res.json({
            data: expedientes,
          })
        } else {
          return res.json({
            message: 'No se encontraron expedientes para el juzgado ',
          })
        }
      } else {
        return res.json({
          message:
            'El identificador de la unidad indicado, no esta registrado ',
        })
      }
    } else {
      res
        .status(500)
        .json({
          message: 'No se recibio el identificador del juzgado',
        })
    }
  } catch (e) {
    res.json({
      message:
        'Indique el identificador del juzgado para retornar la lista de expedientes.',
      data: {},
    })
  }
}

export async function obtenerExpedientes(req, res) {
  try {
    const expedientes = await Expediente.findAll({
      atributes: [
        'cude',
        'idjuzgado',
        'relatohechos',
        'fechahoraingresodenuncia',
        'numerofojasdenuncia',
        'fecharegistro',
      ],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: expedientes,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function eliminarExpediente(req, res) {
  const { idexpediente } = req.params
  try {
    const expediente = await Expediente.findAll({
      atributes: ['idexpediente'],
      where: {
        idexpediente,
        estaactivo: true,
      },
    })
    if (expediente.length > 0) {
      console.log(idexpediente)
      expediente.forEach(async expedienteeliminar => {
        await expedienteeliminar.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'El Expediente fue eliminado',
      })
    } else {
      return res.json({
        message:
          'Expediente no encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Expediente',
      data: {},
    })
  }
}

export async function actualizarExpediente(req, res) {
  const { idexpediente } = req.params
  const { cude, idjuzgado, relatohechos, numerofojasdenuncia } =
    req.body
  try {
    if (idexpediente !== null) {
      const expediente = await Expediente.findAll({
        atributes: ['idexpediente'],
        where: {
          idexpediente,
          estaactivo: true,
        },
      })
      if (expediente.length > 0) {
        let fechahoramodificado = new Date()
        expediente.forEach(async expedienteActualizado => {
          await expedienteActualizado.update({
            cude,
            idjuzgado,
            relatohechos,
            fechahoraingresodenuncia,
            numerofojasdenuncia,
            fechahoramodificado,
          })
        })
        return res.json({
          message: 'Expediente actualizada con éxito',
          data: expediente,
        })
      } else {
        return res.json({
          message:
            'Expediente no encontrado, no existe el identificador o el estado esta inactivo ',
          data: parte,
        })
      }
    } else {
      res.status(500).json({ message: 'Expediente no encontrada' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el expediente',
      data: {},
    })
  }
}
