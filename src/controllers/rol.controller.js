import Rol from '../models/Rol'

export async function nuevoRol(req, res) {
  const { descripcion, fechahoraregistro } = req.body
  try {
    let nuevoRol = await Rol.create(
      {
        descripcion,
        fechahoraregistro,
      },
      {
        fields: ['descripcion', 'fechahoraregistro'],
      }
    )
    if (nuevoRol)
      return res.json({
        message: 'El Rol fue creado',
        data: nuevoRol,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el rol',
      data: {},
    })
  }
}

export async function obtenerRol(req, res) {
  const { idrol } = req.params
  try {
    const rol = await Rol.findOne({
      where: {
        idrol,
      },
    })
    res.json(rol)
  } catch (error) {
    console.log(error)
  }
}

export async function eliminaRol(req, res) {
  const { idrol } = req.params
  try {
    const rol_ = await Rol.findAll({
      atributes: ['idrol'],
      where: {
        idrol,
      },
    })
    if (rol_.length > 0) {
      console.log(rol_.estaactivo)
      rol_.forEach(async rol => {
        await rol.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Rol eliminado',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Rol',
      data: {},
    })
  }
}

export async function actualizarRol(req, res) {
  const { idrol } = req.params
  const { descripcion } = req.body
  const { estaactivo } = req.body
  try {
    const rol = await Rol.findAll({
      atributes: ['idrol', 'descripcion', 'estaactivo'],
      where: {
        idrol,
      },
    })
    if (rol.length > 0) {
      rol.forEach(async rolActualizado => {
        await rolActualizado.update({
          descripcion,
          estaactivo,
        })
      })
      return res.json({
        message: 'Rol Actualizado',
        data: rol,
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede Actualizar el Rol',
      data: {},
    })
  }
}

export async function obtenerRoles(req, res) {
  try {
    const roles = await Rol.findAll({
      atributes: ['idrol', 'descripcion'],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: roles,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}
