'use strict'

import Direccion from '../models/Direccion'
import Persona from '../models/Persona'

import { fechaDeHoy } from '../configfiles/datosExpediente'
import TipoDireccion from '../models/TipoDireccion'

export async function obtenerDireccionesPersona(req, res) {
  //---

  const { idpersona } = req.params
  //console.log(idpersona);

  try {
    if (idpersona !== null) {
      const existepersona = await Persona.findAll({
        where: {
          idpersona: idpersona,
          estaactivo: true,
        },
      })

      if (existepersona.length > 0) {
        const direcciones = await Direccion.findAll({
          where: {
            idpersona: idpersona,
            estaactivo: true,
          },
          include: [
            {
              model: TipoDireccion,
            },
          ],
          order: [['iddireccion', 'DESC']],
        })

        if (direcciones.length > 0) {
          res.json({
            data: direcciones,
          })
        } else {
          res.status(500).json({
            message: 'La persona no tiene direcciones registradas.',
          })
        }
      } else {
        res.status(500).json({
          message:
            'El identificador de la persona, no esta registrado.',
        })
      }
    } else {
      res
        .status(500)
        .json({
          message: 'No se recibio identificador de la persona',
        })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'No se puedo procesar la consulta',
      data: { e },
    })
  }
}

export async function nuevaDireccion(req, res) {
  //----
  const { idpersona, idtipodireccion, descripcion } = req.body

  console.log(req.body)

  try {
    const fechahoraregistro = fechaDeHoy
    const fechahoramodificado = fechaDeHoy

    let nuevaDireccion = await Direccion.create(
      {
        idpersona,
        idtipodireccion,
        descripcion,
        fechahoraregistro,
        fechahoramodificado,
      },
      {
        fields: [
          'idpersona',
          'idtipodireccion',
          'descripcion',
          'fechahoraregistro',
          'estaactivo',
          'fechahoramodificado',
        ],
      }
    )
    if (nuevaDireccion) {
      res.json({
        message: 'Se registro correctamente la dirección de la parte',
        data: nuevaDireccion,
      })
    } else {
      res.status(500).json({
        message: 'La dirección de la parte no pudo ser registrada.',
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'No se puedo procesar la consulta.',
      data: { e },
    })
  }
}

//-------------------------------
