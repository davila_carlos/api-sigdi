import Plantilla from '../models/Plantilla'

export async function nuevaPlantilla(req, res) {
  const { descripcion, archivoactuado, estaactivo } = req.body
  try {
    let nuevaPlantilla = await Plantilla.create(
      {
        //idplantilla,
        descripcion,
        archivoactuado,
        estaactivo,
      },
      {
        fields: ['descripcion', 'archivoactuado', 'estaactivo'],
      }
    )
    if (nuevaPlantilla)
      return res.json({
        message: 'La Plantilla fue creada',
        data: nuevaPlantilla,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar la plantilla',
      data: { e },
    })
  }
}

export async function obtenerPlantilla(req, res) {
  const { idplantilla } = req.params
  try {
    const unaplantilla = await Plantilla.findOne({
      where: {
        idplantilla,
      },
    })
    if (unaplantilla !== null) {
      res.json(unaplantilla)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener la plantilla solicitada',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerPlantillas(req, res) {
  try {
    const plantilla = await Plantilla.findAll({
      atributes: ['descripcion', 'archivoactuado', 'estaactivo'],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: plantilla,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function eliminarPlantilla(req, res) {
  const { idplantilla } = req.params
  try {
    const plantilla = await Plantilla.findAll({
      atributes: ['idplantilla'],
      where: {
        idplantilla,
        estaactivo: true,
      },
    })
    if (plantilla.length > 0) {
      console.log(plantilla.estaactivo)
      plantilla.forEach(async plantilla => {
        await plantilla.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'La plantilla fue eliminada',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar la plantilla',
      data: {},
    })
  }
}

export async function actualizarPlantilla(req, res) {
  const { idplantilla } = req.params
  const { descripcion, archivoactuado } = req.body

  try {
    if (idplantilla !== null) {
      const plantilla = await Plantilla.findAll({
        atributes: ['idplantilla'],
        where: {
          idplantilla,
          estaactivo: true,
        },
      })
      if (plantilla.length > 0) {
        plantilla.forEach(async plantillaActualizada => {
          await plantillaActualizada.update({
            descripcion,
            archivoactuado,
          })
        })
        return res.json({
          message: 'Plantilla actualizada con éxito',
          data: plantilla,
        })
      }
    } else {
      res.status(500).json({ message: 'Plantilla no encontrada' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar la plantilla',
      data: {},
    })
  }
}
