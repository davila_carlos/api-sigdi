import Actuado from '../models/Actuado'

export async function nuevoActuado(req, res) {
  const {
    idexpediente,
    idinstancia,
    idtipoactuado,
    idestado,
    archivoactuado,
    fechahorainicio,
    fechahorafin,
    observacion,
  } = req.body
  try {
    //verificar actuado
    const verificaractuado = await Actuado.findOne({
      where: {
        idinstancia,
        idtipoactuado,
        archivoactuado,
        fechahorainicio,
        observacion,
        estaactivo: true,
      },
    })

    if (verificaractuado === null) {
      let fechahoraregistro = new Date()
      let fechahoramodificado = new Date()
      let nuevoActuado = await Actuado.create(
        {
          //idunidad,
          idexpediente,
          idinstancia,
          idtipoactuado,
          idestado,
          archivoactuado,
          fechahorainicio,
          fechahoraregistro,
          fechahoramodificado,
          fechahorafin,
          observacion,
        },
        {
          fields: [
            'idexpediente',
            'idinstancia',
            'idtipoactuado',
            'idestado',
            'archivoactuado',
            'fechahorainicio',
            'fechahoraregistro',
            'fechahoramodificado',
            'fechahorafin',
            'observacion',
          ],
        }
      )
      if (nuevoActuado)
        return res.json({
          message: 'El actuado fue creado',
          data: nuevaActuado,
        })
    } else {
      return res.json({
        message: 'El actuado ya fue registrado',
        data: nuevaActuado,
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el actuado',
      data: { e },
    })
  }
}

export async function obtenerActuado(req, res) {
  const { idactuado } = req.params
  try {
    const unaactuado = await Actuado.findOne({
      where: {
        idactuado,
      },
    })
    if (unactuado !== null) {
      res.json(unactuado)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener actuado solicitado',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerActuados(req, res) {
  try {
    const actuado = await Actuado.findAll({
      atributes: [
        'idactuado',
        'idexpediente',
        'idinstancia',
        'idtipoactuado',
        'idestado',
        'archivoactuado',
        'fechahoraregistro',
        'fechahorainicio',
        'fechahorafin',
        'observacion',
        'estaactivo',
      ],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: actuado,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function eliminarActuado(req, res) {
  const { idactuado } = req.params
  try {
    const actuado = await Actuado.findAll({
      atributes: ['idactuado'],
      where: {
        idactuado,
        estaactivo: true,
      },
    })
    if (actuado.length > 0) {
      console.log(actuado.estaactivo)
      actuado.forEach(async eliminaactuado => {
        await eliminaactuado.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'El actuado fue eliminado',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el actuado',
      data: {},
    })
  }
}

export async function actualizarActuado(req, res) {
  const { idactuado } = req.params
  const {
    idinstancia,
    idtipoactuado,
    idestado,
    archivoactuado,
    fechahorainicio,
    fechahorafin,
    observacion,
  } = req.body

  try {
    if (idactuado !== null) {
      const actuado = await Actuado.findAll({
        atributes: ['idactuado'],
        where: {
          idactuado,
          estaactivo: true,
        },
      })
      if (actuado.length > 0) {
        let fechahoramodificado = new Date()
        actuado.forEach(async actuadoActualizado => {
          await actuadoActualizado.update({
            idexpediente,
            idinstancia,
            idtipoactuado,
            idestado,
            archivoactuado,
            fechahoraregistro,
            fechahorainicio,
            fechahorafin,
            fechahoramodificado,
            observacion,
          })
        })
        return res.json({
          message: 'Actuado actualizado con éxito',
          data: actuado,
        })
      } else {
        return res.json({
          message:
            'Actuado no encontrado, no existe el identificador o el estado esta inactivo ',
          data: actuado,
        })
      }
    } else {
      res.status(500).json({
        message: 'Actuado no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el actuado',
      data: {},
    })
  }
}
