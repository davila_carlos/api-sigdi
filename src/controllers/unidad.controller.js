import Unidad from '../models/Unidad'
import TipoUnidad from '../models/TipoUnidad'
import Departamento from '../models/Departamento'
import Localidad from '../models/Localidad'
import Institucion from '../models/Institucion'

export async function obtenerUnidades(req, res) {
  //----
  try {
    const unidad = await Unidad.findAll({
      //atributes: ['idunidad', 'idunidadsinaes', 'sigla', 'nombreunidadsinaes', 'estaactivo'],
      where: {
        estaactivo: true,
      },
      include: [
        {
          model: TipoUnidad,
        },
        {
          model: Departamento,
        },
        {
          model: Localidad,
        },
        {
          model: Institucion,
        },
      ],
    })
    if (unidad.length > 0) {
      res.json({
        data: unidad,
      })
    } else {
      res.status(500).json({
        message: 'No se tiene unidades registradas',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta',
    })
  }
}

//-------------------------------------------------

export async function nuevaUnidad(req, res) {
  const { idunidadsinaes, sigla, nombreunidadsinaes, estaactivo } =
    req.body
  try {
    let nuevaUnidad = await Unidad.create(
      {
        //idunidad,
        idunidadsinaes,
        sigla,
        nombreunidadsinaes,
        estaactivo,
      },
      {
        fields: [
          'idunidadsinaes',
          'sigla',
          'nombreunidadsinaes',
          'estaactivo',
        ],
      }
    )
    if (nuevaUnidad)
      return res.json({
        message: 'La Unidad fue creada',
        data: nuevaUnidad,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar la unidad',
      data: { e },
    })
  }
}

export async function obtenerUnidad(req, res) {
  const { idunidad } = req.params
  try {
    const unaunidad = await Unidad.findOne({
      where: {
        idunidad,
      },
    })
    if (unaunidad !== null) {
      res.json(unaunidad)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener la unidad solicitada',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function eliminarUnidad(req, res) {
  const { idunidad } = req.params
  try {
    const unidad = await Unidad.findAll({
      atributes: ['idunidad'],
      where: {
        idunidad,
        estaactivo: true,
      },
    })
    if (unidad.length > 0) {
      console.log(unidad.estaactivo)
      unidad.forEach(async unidad => {
        await unidad.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'La unidad fue eliminada',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar la unidad',
      data: {},
    })
  }
}

export async function actualizarUnidad(req, res) {
  const { idunidad } = req.params
  const { idunidadsinaes, sigla, nombreunidadsinaes } = req.body

  try {
    if (idunidad !== null) {
      const unidad = await Unidad.findAll({
        atributes: ['idunidad'],
        where: {
          idunidad,
          estaactivo: true,
        },
      })
      if (unidad.length > 0) {
        unidad.forEach(async unidadActualizada => {
          await unidadActualizada.update({
            idunidadsinaes,
            sigla,
            nombreunidadsinaes,
          })
        })
        return res.json({
          message: 'Unidad actualizada con éxito',
          data: unidad,
        })
      }
    } else {
      res.status(500).json({ message: 'Unidad no encontrada' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar la unidad',
      data: {},
    })
  }
}
