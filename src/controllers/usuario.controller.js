require('dotenv').config()

import sendEmail from '../sendEmail' // sendEmail para la confirmacion de la cuenta
import { generateToken, validateToken } from '../tokenGenerator' // para generar los tokens

import { QueryTypes } from 'sequelize'
import { sequelize } from '../database/database'
import Usuario from '../models/Usuario'

export async function nuevoUsuario(req, res) {
  const {
    idunidad,
    idrol,
    nombre,
    paterno,
    materno,
    ci,
    complemento,
    idexpedido,
    fechanacimiento,
    email,
    direccion,
    telefono,
    telefonooficina,
  } = req.body
  try {
    let nuevoUsuario = await Usuario.create(
      {
        idunidad,
        idrol,
        nombre,
        paterno,
        materno,
        ci,
        complemento,
        idexpedido,
        fechanacimiento,
        email,
        direccion,
        telefono,
        telefonooficina,
      },
      {
        fields: [
          'idunidad',
          'idrol',
          'nombre',
          'paterno',
          'materno',
          'ci',
          'complemento',
          'idexpedido',
          'fechanacimiento',
          'email',
          'direccion',
          'telefono',
          'telefonooficina',
        ],
      }
    )
    if (nuevoUsuario) {
      // Generación del Token
      let token = generateToken(nuevoUsuario.idusuario)
      console.log(token)
      // Envio de correo de confirmacion (to, subject, name, html)
      sendEmail(
        req.body.email,
        'Confirma tu cuenta',
        `${req.body.nombre} ${req.body.paterno} ${req.body.materno}`,
        token
      )

      return res.json({
        message: 'Un email ha sido enviado para confirmar tu cuenta',
        data: nuevoUsuario,
      })
    }
  } catch (e) {
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el usuario',
      data: { e },
    })
  }
}

// Esta funcion valida el token del usuario
export async function confirmToken(req, res) {
  let token = req.params.token
  let confirmation = validateToken(token)
  if (confirmation) {
    // Actualizar estaactivo a true
    try {
      const result = await Usuario.update(
        { estaactivo: true },
        { where: { idusuario: confirmation.id } }
      )
      return res.status(200).json({
        message: 'Estado actualizado',
        result: result,
      })
    } catch (err) {
      return res.status(403).json({
        message: err,
      })
    }
  } else {
    res.status(403).json({
      message: 'El token no es valido',
    })
  }
}

export async function obtenerUsuario(req, res) {
  const { idusuario } = req.params
  try {
    const user = await Usuario.findOne({
      where: {
        idusuario,
      },
    })
    if (user) {
      res.json(user)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener el usuario solicitado',
      })
    }
  } catch (err) {
    res.status(403).json({
      message: `Ha ocurrido un error ${err}`,
    })
  }
}

export async function eliminaUsuario(req, res) {
  const { idusuario } = req.params
  try {
    const user = await Usuario.findOne({
      where: {
        idusuario,
      },
    })
    if (user) {
      user.update({ estaactivo: false })
      return res.json({
        message: 'El usuario fue eliminado',
      })
    } else {
      res.status(404).json({
        message: 'Usuario no encontrado',
      })
    }
  } catch (e) {
    res.status(500).json({
      message: 'Ha ocurrido un error al buscar al usuario',
    })
  }
}

export async function actualizarUsuario(req, res) {
  const { idusuario } = req.params
  const {
    usuario,
    idunidad,
    idrol,
    nombre,
    paterno,
    materno,
    ci,
    complemento,
    idexpedido,
    fechanacimiento,
    email,
    direccion,
    telefono,
    telefonooficina,
    fechahoraregistro,
    estaactivo,
  } = req.body
  try {
    const user = await Usuario.findOne({
      where: {
        idusuario,
      },
    })
    if (user) {
      user.update({
        usuario: usuario,
        idunidad: idunidad,
        idrol: idrol,
        nombre: nombre,
        paterno: paterno,
        materno: materno,
        ci: ci,
        complemento: complemento,
        idexpedido: idexpedido,
        fechanacimiento: fechanacimiento,
        email: email,
        direccion: direccion,
        telefono: telefono,
        telefonooficina: telefonooficina,
        fechahoraregisto: fechahoraregistro,
        estaactivo: estaactivo,
      })
      return res.json({
        message: 'Los datos del usuario han sido actualizados',
        data: user,
      })
    } else {
      res.status(404).json({
        message: 'Usuario no encontrado',
      })
    }
  } catch (e) {
    res.status(500).json({
      message: 'Ha ocurrido un error al buscar ese usuario',
    })
  }
}

export async function obtenerUsuarios(req, res) {
  try {
    const usuarios = await Usuario.findAll({
      atributes: [
        'idusuario',
        'usuario',
        'clave',
        'idunidad',
        'idrol',
        'nombre',
        'paterno',
        'materno',
        'ci',
        'complemento',
        'idexpedido',
        'fechanacimiento',
        'email',
        'direccion',
        'telefono',
        'telefonooficina',
        'fechahoraregistro',
      ],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: usuarios,
    })
  } catch (error) {
    res.json({
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function realizarLogin(req, res) {
  try {
    const usuariolog = await sequelize.query(
      //('select * from rol',// select fn_login(\'cherbas\', \'5681454
      'select fn_login(:username, :userpass)',
      {
        replacements: {
          username: req.body.username,
          userpass: req.body.userpass,
        },
        type: QueryTypes.SELECT,
      }
    )
    res.json(usuariolog)
  } catch (err) {
    res.json({
      message: 'sin datos',
    })
  }
}

export async function obtenerLogueo(req, res) {
  try {
    const usuariolog = await sequelize.query(
      //('select * from rol',// select fn_login(\'cherbas\', \'5681454)
      'select fn_logueado(:username, :userpass)',
      {
        replacements: {
          username: req.body.username,
          userpass: req.body.userpass,
        },
        type: QueryTypes.SELECT,
      }
    )
    res.json(usuariolog[0]['fn_logueado'][0])
  } catch (err) {
    res.status(500).json({
      message: 'sin datos',
    })
  }
}

export async function cambiarClavePersona(req, res) {
  try {
    const usuariolog = await sequelize.query(
      //('select * from rol',// select fn_login(\'cherbas\', \'5681454
      'select fn_cambiarClavePersona(:iduser, :userpass, :nuevouserpass)',
      {
        replacements: {
          iduser: req.body.iduser,
          userpass: req.body.userpass,
          nuevouserpass: req.body.nuevouserpass,
        },
        type: QueryTypes.SELECT,
      }
    )
    res.json(usuariolog[0]['fn_cambiarclavepersona'][0])
  } catch (err) {
    res.status(500).json({
      message: 'sin datos',
    })
  }
}

export async function resetearClave(req, res) {
  try {
    const usuariolog = await sequelize.query(
      //('select * from rol',// select fn_login(\'cherbas\', \'5681454
      'select fn_resetearClave(:iduser)',
      {
        replacements: {
          iduser: req.body.iduser,
        },
        type: QueryTypes.SELECT,
      }
    )
    res.json(usuariolog[0]['fn_resetearclave'][0])
  } catch (err) {
    res.status(500).json({
      message: 'sin datos',
    })
  }
}
