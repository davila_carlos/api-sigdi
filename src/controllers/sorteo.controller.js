import Sorteo from '../models/Sorteo'

export async function nuevoSorteo(req, res) {
  const { idexpediente, idjuzgado, fechahoraregistro, estaactivo } =
    req.body
  console.log('unoooooo')
  try {
    console.log('doooos')
    let nuevoSorteo = await Sorteo.create(
      {
        idexpediente,
        idjuzgado,
        fechahoraregistro,
        estaactivo,
      },
      {
        fields: [
          'idexpediente',
          'idjuzgado',
          'fechahoraregistro',
          'estaactivo',
        ],
      }
    )
    if (nuevoSorteo)
      return res.json({
        message: 'Se regitró el sorteo',
        data: nuevoSorteo,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el sorteo',
      data: { e },
    })
  }
}

export async function obtenerSorteo(req, res) {
  const { idsorteo } = req.params
  try {
    const ununsorteo = await Sorteo.findOne({
      where: {
        idsorteo,
      },
    })
    if (ununsorteo !== null) {
      res.json(ununsorteo)
    } else {
      res.status(500).json({
        message: 'Hubo un error, no podemos obtener el sorteo',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerSorteos(req, res) {
  try {
    const sorteo = await Sorteo.findAll({
      atributes: [
        'idsorteo',
        'idexpediente',
        'idjuzgado',
        'fechahoraregistro',
        'estaactivo',
      ],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: sorteo,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function eliminarSorteo(req, res) {
  const { idsorteo } = req.params
  try {
    const sorteo = await Sorteo.findAll({
      atributes: ['idsorteo'],
      where: {
        idsorteo,
        estaactivo: true,
      },
    })
    if (sorteo.length > 0) {
      console.log(sorteo.estaactivo)
      sorteo.forEach(async sorteo => {
        await sorteo.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'El sorteo fue eliminado',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el sorteo',
      data: {},
    })
  }
}

export async function actualizarSorteo(req, res) {
  const { idsorteo } = req.params
  const { idexpediente, idjuzgado, fechahoraregistro, estaactivo } =
    req.body

  try {
    if (idsorteo !== null) {
      const sorteo = await Sorteo.findAll({
        atributes: ['idsorteo'],
        where: {
          idsorteo,
          estaactivo: true,
        },
      })
      if (sorteo.length > 0) {
        sorteo.forEach(async sorteoActualizado => {
          await sorteoActualizado.update({
            idexpediente,
            idjuzgado,
            fechahoraregistro,
            estaactivo,
          })
        })
        return res.json({
          message: 'Sorteo actualizado con éxito',
          data: sorteo,
        })
      }
    } else {
      res.status(500).json({ message: 'Sorteo no encontrado' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el sorteo',
      data: {},
    })
  }
}
