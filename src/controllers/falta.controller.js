'use strict'

import Falta from '../models/Falta'
import TipoFalta from '../models/TipoFalta'
//import { actualizarPersona } from './persona.controller';

export async function obtenerFaltasDeTipoFalta(req, res) {
  const { idtipofalta } = req.params

  try {
    //---------verificando idexpediente----------------
    const existeTipoFalta = await TipoFalta.findAll({
      where: {
        idtipofalta: idtipofalta,
        estaactivo: true,
      },
    })

    if (existeTipoFalta.length > 0) {
      const faltas = await Falta.findAll({
        where: {
          idtipofalta: idtipofalta,
          estaactivo: true,
        },
        order: [['descripcion', 'ASC']],
      })

      if (faltas.length > 0) {
        res.json({ data: faltas })
      } else {
        res.status(500).json({
          message:
            'Hubo un error, no se pudo eobtener la  lista de Faltas',
        })
      }
    } else {
      let $mensajeError = '.'
      if (existeTipoFalta.length == 0) {
        $mensajeError = 'El identificador del TipoFalta no existe'
        console.log('El identificador del TipoFalta no existe')
      }

      res.status(500).json({
        message: `No se puede realizar el registro de la parte: ${$mensajeError}`,
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta.',
    })
  }
}

//-----------------------

export async function nuevaFalta(req, res) {
  const { descripcion, idtipofalta } = req.body

  console.log(req.body)

  try {
    let nuevaFalta = await Falta.create(
      {
        descripcion,
        idtipofalta,
      },
      {
        fields: ['descripcion', 'idtipofalta'],
      }
    )
    if (nuevaFalta)
      return res.json({
        message: 'La falta fue creado',
        data: nuevaFalta,
      })
  } catch (e) {
    console.log(e.message)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registrar la Falta',
      data: { e },
    })
  }
}

export async function obtenerFalta(req, res) {
  const { idfalta } = req.params

  try {
    const falta = await Falta.findOne({
      where: {
        idfalta,
      },
    })

    if (idfalta !== null) {
      res.json(falta)
    } else {
      res.status(500).json({
        message: 'Hubo un error, no se pudo encontrar la Falta',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerFaltas(req, res) {
  try {
    const falta = await Falta.findAll({
      atributes: [
        'idfalta',
        'descripcion',
        'estaactivo',
        'idtipofalta',
      ],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: falta,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function actualizarFalta(req, res) {
  const { idfalta } = req.params

  const { descripcion, idtipofalta } = req.body

  try {
    if (idfalta !== null) {
      const falta = await Falta.findAll({
        atributes: ['idfalta'],
        where: {
          idfalta,
          estaactivo: true,
        },
      })
      if (falta.length > 0) {
        falta.forEach(async faltaactualizada => {
          await faltaactualizada.update({
            descripcion,
            idtipofalta,
          })
        })
        return res.json({
          message: 'Falta actualizado con éxito',
          data: falta,
        })
      } else {
        return res.json({
          message:
            'Falta No encontrado, no existe el identificador o el estado esta inactivo ',
          data: falta,
        })
      }
    } else {
      res.status(500).json({
        message: 'Falta no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar la Falta',
      data: {},
    })
  }
}

export async function eliminaFalta(req, res) {
  const { idfalta } = req.params
  try {
    const falta = await Falta.findAll({
      atributes: ['idfalta'],
      where: {
        idfalta,
        estaactivo: true,
      },
    })
    if (falta.length > 0) {
      console.log(falta.estaactivo)
      falta.forEach(async actualizarfalta => {
        await actualizarfalta.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Falta eliminado.',
      })
    } else {
      return res.json({
        message:
          'Falta No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar la Falta.',
      data: {},
    })
  }
}

export async function habilitarFalta(req, res) {
  const { idfalta } = req.params
  try {
    const falta = await Falta.findAll({
      atributes: ['idfalta'],
      where: {
        idfalta,
        estaactivo: false,
      },
    })
    if (falta.length > 0) {
      console.log(falta.estaactivo)
      falta.forEach(async actualizarfalta => {
        await actualizarfalta.update({
          estaactivo: true,
        })
      })
      return res.json({
        message: 'Falta Habilitado.',
      })
    } else {
      return res.json({
        message:
          'Falta No encontrado, no existe el identificador o el estado esta activo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede habilitar la Falta.',
      data: {},
    })
  }
}
