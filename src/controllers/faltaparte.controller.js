'use strict'

import FaltaParte from '../models/FaltaParte'
import Parte from '../models/Parte'
import Falta from '../models/Falta'

import { fechaDeHoy } from '../configfiles/datosExpediente'

export async function nuevaFaltaParte(req, res) {
  const { idparte, idfalta, descripciondenuncia } = req.body
  console.log(req.body)

  try {
    //---------verificando idparte----------------
    const existeparte = await Parte.findAll({
      where: {
        idparte: idparte,
        estaactivo: true,
      },
    })

    //---------verificando idfalta----------------
    const existefalta = await Falta.findAll({
      where: {
        idfalta: idfalta,
        estaactivo: true,
      },
    })

    if (existeparte.length > 0 && existefalta.length > 0) {
      let fechahoraregistro = fechaDeHoy
      let fechahoramodificado = fechaDeHoy

      let nuevaFaltaParte = await FaltaParte.create(
        {
          idparte,
          idfalta,
          descripciondenuncia,
          fechahoraregistro,
          fechahoramodificado,
        },
        {
          fields: [
            'idparte',
            'idfalta',
            'descripciondenuncia',
            'fechahoraregistro',
            'fechahoramodificado',
          ],
        }
      )
      if (nuevaFaltaParte) {
        res.json({
          message:
            'La falta de la parte fue registrada correctamente.',
          data: nuevaFaltaParte,
        })
      } else {
        res.status(500).json({
          message:
            'La falta de la parte fue registrada correctamente.',
        })
      }
    } else {
      let $mensajeError = '.'
      if (existeparte.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador de la parte no existe'
        console.log('El identificador de la parte no existe')
      }
      if (existefalta.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador de la falta no existe'
        console.log('El identificador de la falta no existe')
      }

      res.status(500).json({
        message: `No se puede realizar el registro de la falta para la parte: ${$mensajeError}`,
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar la Falta Parte',
      data: { e },
    })
  }
}

export async function obtenerFaltasdelaParte(req, res) {
  //---

  const { idparte } = req.params

  try {
    //---------verificando idparte----------------
    const existeparte = await Parte.findAll({
      where: {
        idparte: idparte,
        estaactivo: true,
      },
    })

    if (existeparte.length > 0) {
      const faltasparte = await FaltaParte.findAll({
        where: {
          idparte,
          estaactivo: true,
        },
        include: [
          {
            model: Parte,
          },
          {
            model: Falta,
          },
        ],
      })
      if (faltasparte.length > 0) {
        res.json({
          data: faltasparte,
        })
      } else {
        res.status(500).json({
          message: 'No se puedieron encontrar Faltas para la parte',
        })
      }
    } else {
      let $mensajeError = '.'
      if (existeparte.length == 0) {
        $mensajeError = 'El identificador de la parte no existe'
        console.log('El identificador de la parte no existe')
      }

      res.status(500).json({
        message: `No se puede realizar el registro de la falta para la parte: ${$mensajeError}`,
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

//-------------------------------
