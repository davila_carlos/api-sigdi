'use strict'

import Cargo from '../models/Cargo'

export async function obtenerCargos(req, res) {
  try {
    const cargos = await Cargo.findAll({
      //---
      atributes: ['idcargo', 'descripcion', 'estaactivo'],
      where: {
        estaactivo: true,
      },
    })

    if (cargos.length > 0) {
      res.json({
        data: cargos,
      })
    } else {
      res.status(500).json({
        message: 'No se registros de cargos.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: {},
      message: 'No se pudo obtener la lista de cargos',
    })
  }
}
