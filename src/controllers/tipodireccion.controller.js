'use strict'

import TipoDireccion from '../models/TipoDireccion'

export async function obtenerTiposDirecciones(req, res) {
  try {
    const tipodirecciones = await TipoDireccion.findAll({
      where: {
        estaactivo: true,
      },
      order: [['descripcion', 'ASC']],
    })
    if (tipodirecciones.length > 0) {
      res.json({
        data: tipodirecciones,
      })
    } else {
      res.status(500).json({
        message: 'No se tienen tipos de direcciones registradas.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta.',
    })
  }
}

//----------------------------------------

export async function nuevoTipoDireccion(req, res) {
  const { descripcion } = req.body

  try {
    let nuevoTipoDireccion = await TipoDireccion.create(
      {
        descripcion,
      },
      {
        fields: ['descripcion'],
      }
    )
    if (nuevoTipoDireccion)
      return res.json({
        message: 'El Tipo de Dirección fue creado',
        data: nuevoTipoDireccion,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message:
        'Hubo un error, no se pudo registar el Tipo de DIreccion',
      data: { e },
    })
  }
}

export async function obtenerTipoDireccion(req, res) {
  const { idtipodireccion } = req.params

  try {
    const tipoDireccion = await TipoDireccion.findOne({
      where: {
        idtipodireccion,
      },
    })

    if (tipoDireccion !== null) {
      res.json(tipoDireccion)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no se pudo encontrar el tipo de dirección',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function actualizarTipoDireccion(req, res) {
  const { idtipodireccion } = req.params

  const { descripcion } = req.body

  try {
    if (idtipodireccion !== null) {
      const tipodireccion = await TipoDireccion.findAll({
        //atributes: ['idtipodireccion', 'descripcion'],
        atributes: ['idtipodireccion'],
        where: {
          idtipodireccion,
          estaactivo: true,
        },
      })
      if (tipodireccion.length > 0) {
        tipodireccion.forEach(async tipodireccionactualizada => {
          await tipodireccionactualizada.update({
            descripcion,
          })
        })
        return res.json({
          message: 'Tipo de Dirección actualizado con éxito',
          data: tipodireccion,
        })
      } else {
        return res.json({
          message:
            'Tipo Dirección No encontrado, no existe el identificador o el estado esta inactivo ',
          data: tipodireccion,
        })
      }
    } else {
      res.status(500).json({
        message: 'Tipo Dirección no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el tipo de direccion',
      data: {},
    })
  }
}

export async function eliminaTipoDireccion(req, res) {
  const { idtipodireccion } = req.params
  try {
    const tipodireccion = await TipoDireccion.findAll({
      atributes: ['idtipodireccion'],
      where: {
        idtipodireccion,
        estaactivo: true,
      },
    })
    if (tipodireccion.length > 0) {
      console.log(tipodireccion.estaactivo)
      tipodireccion.forEach(async tipoDireccion => {
        await tipoDireccion.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Tipo Dirección eliminado.',
      })
    } else {
      return res.json({
        message:
          'Tipo Dirección No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Tipo de Dirección.',
      data: {},
    })
  }
}

export async function habilitarTipoDireccion(req, res) {
  const { idtipodireccion } = req.params
  try {
    const tipodireccion = await TipoDireccion.findAll({
      atributes: ['idtipodireccion'],
      where: {
        idtipodireccion,
        estaactivo: false,
      },
    })
    if (tipodireccion.length > 0) {
      console.log(tipodireccion.estaactivo)
      tipodireccion.forEach(async tipoDireccion => {
        await tipoDireccion.update({
          estaactivo: true,
        })
      })
      return res.json({
        message: 'Tipo Dirección Habilitado.',
      })
    } else {
      return res.json({
        message:
          'Tipo Dirección No encontrado, no existe el identificador o el estado esta activo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede habilitar el Tipo de Dirección.',
      data: {},
    })
  }
}
