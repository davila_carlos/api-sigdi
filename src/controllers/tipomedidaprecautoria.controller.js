'use strict'

import TipoMedidaPrecautoria from '../models/TipoMedidaPrecautoria'

export async function obtenerTipoMedidaPrecautorias(req, res) {
  //----
  try {
    const tipomedidasprecautorias =
      await TipoMedidaPrecautoria.findAll({
        where: {
          estaactivo: true,
        },
        order: [['descripcion', 'ASC']],
      })

    if (tipomedidasprecautorias.length > 0) {
      res.json({
        data: tipomedidasprecautorias,
      })
    } else {
      res.status(500).json({
        message: 'No se tiene registros de medidas precautorias',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      message: 'No se puedo procesar la consulta',
      data: { error },
    })
  }
}

//-----------------------
