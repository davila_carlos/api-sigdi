'use strict'

import Expediente from '../models/Expediente'
import Unidad from '../models/Unidad'
import Tipomedidaprecautoria from '../models/TipoMedidaPrecautoria'

import {
  rutaArchivo,
  fechaDeHoy,
} from '../configfiles/datosExpediente'
import Parte from '../models/Parte'

export async function obtenerExpedientes(req, res) {
  try {
    const expedientes = await Expediente.findAll({
      where: {
        estaactivo: true,
      },
      include: [
        {
          model: Unidad,
        },
        {
          model: Tipomedidaprecautoria,
        },
      ],
      order: [['idexpediente', 'DESC']],
    })

    if (expedientes.length > 0) {
      res.json({
        data: expedientes,
      })
    } else {
      res.status(500).json({
        message: 'No se tiene expedientes registrados.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      message: 'No se puedo procesar la consulta',
    })
  }
}

export async function nuevoExpediente(req, res) {
  const {
    idtipomedidaprecautoria,
    idjuzgado,
    relatohechos,
    fechahoraingresodenuncia,
    numerofojasdenuncia,
  } = req.body

  try {
    const existeunidad = await Unidad.findAll({
      where: {
        idunidad: idjuzgado,
        estaactivo: true,
      },
    })

    if (existeunidad.length > 0) {
      const existenexpedientes = await Expediente.findOne({
        where: {
          estaactivo: true,
        },
        order: [['idexpediente', 'DESC']],
      })

      const gestionactual = fechaDeHoy.getFullYear()
      let ultcude = 0
      let obtinc1 = 0
      let obtinc2 = 0

      if (existenexpedientes) {
        ultcude = String(existenexpedientes.cude)
        let obtgestion = ultcude.slice(0, -5)
        obtinc1 = ultcude.slice(4, -3)
        obtinc2 = ultcude.slice(6)

        let aux = 0

        if (gestionactual == obtgestion) {
          if (obtinc2 < 999) {
            aux = String(Number(obtinc2) + 1)

            if (aux.length == 1) {
              obtinc2 = '00' + aux
            } else {
              if (aux.length == 2) {
                obtinc2 = '0' + aux
              } else {
                if (aux.length == 3) {
                  obtinc2 = aux
                }
              }
            }
          } else {
            if (Number(obtinc1) < 99) {
              aux = String(Number(obtinc1) + 1)
              if (aux.length == 1) {
                obtinc1 = '0' + aux
              } else {
                if (aux.length == 2) {
                  obtinc1 = aux
                }
              }
              obtinc2 = '001'
            } else {
              console.log(
                'El INC1 no puede volver a tener el valor 01 en la misma gestion.'
              )
            }
          }
          ultcude = obtgestion + obtinc1 + obtinc2
          console.log(
            'cude generado siguiente registro misma gestion:' +
              ultcude
          )
        } else {
          obtinc1 = '01'
          obtinc2 = '001'
          ultcude = gestionactual + obtinc1 + obtinc2
          console.log('cude generado nueva gestion: ' + ultcude)
        }
      } else {
        obtinc1 = '01'
        obtinc2 = '001'
        ultcude = gestionactual + obtinc1 + obtinc2
        console.log('cude generado primer expediente: ' + ultcude)
      }

      const fechahoraregistro = fechaDeHoy
      const fechahoramodificado = fechaDeHoy
      //const relatohechos = nombreArchivo + '_' + req.file.originalname
      const cude = ultcude

      const nuevoExpediente = await Expediente.create(
        {
          cude,
          idtipomedidaprecautoria,
          idjuzgado,
          relatohechos,
          fechahoraingresodenuncia,
          numerofojasdenuncia,
          fechahoraregistro,
          fechahoramodificado,
        },
        {
          fields: [
            'cude',
            'idtipomedidaprecautoria',
            'idjuzgado',
            'relatohechos',
            'fechahoraingresodenuncia',
            'numerofojasdenuncia',
            'fechahoraregistro',
            'fechahoramodificado',
          ],
        }
      )

      if (nuevoExpediente) {
        res.json({
          message: 'El expediente fue registrado correctamente.',
          data: nuevoExpediente,
        })
      } else {
        res.status(500).json({
          message: 'El expediente no pudo ser registrado.',
        })
      }
    } else {
      res.status(500).json({
        message: 'El juzgado no esta registrado en el sistema.',
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'No se pudo procesar la consulta',
      data: { e },
    })
  }
}

export async function obtenerArchivoExpediente(req, res) {
  try {
    res.download(
      rutaArchivo + '/' + req.params.relatohechos,
      req.params.relatohechos
    )
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'No se pudo obtener el archivo.',
      data: { e },
    })
  }
}

export async function obtenerExpediente(req, res) {
  const { idexpediente } = req.params

  try {
    const expediente = await Expediente.findOne({
      where: {
        idexpediente,
        estaactivo: true,
      },
      include: [
        {
          model: Unidad,
        },
        {
          model: Tipomedidaprecautoria,
        },
      ],
    })

    if (expediente !== null) {
      res.json({
        data: expediente,
      })
    } else {
      res.status(500).json({
        message:
          'No se tiene registros de expediente con el identificador proporcionado.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta',
    })
  }
}

export async function obtenerExpedientesJuzgado(req, res) {
  const { idunidad } = req.params
  console.log(idunidad)
  try {
    if (idunidad !== null) {
      const existeunidad = await Unidad.findAll({
        where: {
          idunidad: idunidad,
          estaactivo: true,
        },
      })
      if (existeunidad.length > 0) {
        const expedientes = await Expediente.findAll({
          where: { idjuzgado: idunidad },
          include: [
            {
              model: Unidad,
              where: { idunidad: idunidad },
            },
          ],
          order: [['idexpediente', 'DESC']],
        })

        if (expedientes.length > 0) {
          res.json({
            data: expedientes,
          })
        } else {
          return res.json({
            message: 'No se encontraron expedientes para la unidad ',
          })
        }
      } else {
        return res.json({
          message:
            'El identificador de la unidad, no esta registrado ',
        })
      }
    } else {
      res.status(500).json({
        message: 'No se recibio el identificador de la unidad',
      })
    }
  } catch (e) {
    res.json({
      message:
        'Indique el identificador de la unidad para retornar la lista de expedientes.',
      data: {},
    })
  }
}

export async function eliminarExpediente(req, res) {
  const { idexpediente } = req.params
  try {
    const expediente = await Expediente.findAll({
      atributes: ['idexpediente'],
      where: {
        idexpediente,
        estaactivo: true,
      },
    })
    if (expediente.length > 0) {
      console.log(idexpediente)
      expediente.forEach(async expedienteeliminar => {
        await expedienteeliminar.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'El Expediente fue eliminado',
      })
    } else {
      return res.json({
        message:
          'Expediente no encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Expediente',
      data: {},
    })
  }
}

export async function actualizarExpediente(req, res) {
  const { idexpediente } = req.params
  const {
    cude,
    idjuzgado,
    relatohechos,
    fechahoraingresodenuncia,
    numerofojasdenuncia,
  } = req.body
  try {
    const expediente = await Expediente.findOne({
      where: {
        idexpediente,
      },
    })
    if (expediente) {
      let fechahoramodificado = new Date()
      await expediente.update({
        cude,
        idjuzgado,
        relatohechos,
        fechahoraingresodenuncia,
        numerofojasdenuncia,
        fechahoramodificado,
      })
      return res.json({
        message: 'Expediente actualizada con éxito',
        data: expediente,
      })
    } else {
      res.status(500).json({ message: 'Expediente no encontrada' })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el expediente',
      data: {},
    })
  }
}

export async function obtenerExpedientesPersona(req, res) {
  const { idpersona } = req.params
  console.log(idpersona)
  try {
    if (idpersona !== null) {
      const expedientes = await Parte.findAll({
        where: { idpersona: idpersona },
        include: [
          {
            model: Expediente,
          },
        ],
        order: [['idpersona', 'DESC']],
      })
      
      if (expedientes.length > 0) {
        console.log('expe')
        res.json({
          data: expedientes,
        })
      } else {
        return res.json({
          message: 'No se encontraron expedientes para la unidad ',
        })
      }
    } else {
      res.status(500).json({
        message: 'No se recibio el identificador de la unidad',
      })
    }
  } catch (e) {
    res.json({
      message:
        'Indique el identificador de la unidad para retornar la lista de expedientes.',
      data: {},
    })
  }
}
