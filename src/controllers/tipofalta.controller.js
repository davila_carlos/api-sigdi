'use strict'

import TipoFalta from '../models/TipoFalta'

export async function obtenerTiposFaltas(req, res) {
  //---

  try {
    const tiposfaltas = await TipoFalta.findAll({
      where: {
        estaactivo: true,
      },
      order: [['descripcion', 'ASC']],
    })
    //console.log(tipofalta.length);

    if (tiposfaltas.length > 0) {
      res.json({
        data: tiposfaltas,
      })
    } else {
      res.status(500).json({
        message: 'No se tiene registros de tipo parte.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta.',
    })
  }
}

//--------------------------------------------------------------------

export async function nuevoTipoFalta(req, res) {
  const { descripcion } = req.body

  try {
    let nuevoTipoFalta = await TipoFalta.create(
      {
        descripcion,
      },
      {
        fields: ['descripcion'],
      }
    )
    if (nuevoTipoFalta)
      return res.json({
        message: 'El Tipo de Falta fue creado',
        data: nuevoTipoFalta,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el Tipo de Falta',
      data: { e },
    })
  }
}

export async function obtenerTipoFalta(req, res) {
  const { idtipofalta } = req.params

  try {
    const tipoFalta = await TipoFalta.findOne({
      where: {
        idtipofalta,
      },
    })

    if (tipoFalta !== null) {
      res.json(tipoFalta)
    } else {
      res.status(500).json({
        message: 'Error, El tipo de Falta no se encontró',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function actualizarTipoFalta(req, res) {
  const { idtipofalta } = req.params

  const { descripcion } = req.body
  console.log(req.body)

  try {
    if (idtipofalta !== null) {
      const tipofalta = await TipoFalta.findAll({
        //atributes: ['idtipodireccion', 'descripcion'],
        atributes: ['idtipofalta'],
        where: {
          idtipofalta,
          estaactivo: true,
        },
      })
      if (tipofalta.length > 0) {
        tipofalta.forEach(async tipofaltaactualizada => {
          await tipofaltaactualizada.update({
            descripcion,
          })
        })
        return res.json({
          message: 'Tipo de Dirección actualizado con éxito',
          data: tipofalta,
        })
      } else {
        return res.json({
          message:
            'Tipo Falta No encontrado, no existe el identificador o el estado esta inactivo',
          data: tipofalta,
        })
      }
    } else {
      res.status(500).json({
        message: 'Tipo Falta no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el tipo de falta',
      data: {},
    })
  }
}

export async function eliminaTipoFalta(req, res) {
  const { idtipofalta } = req.params
  try {
    const tipofalta = await TipoFalta.findAll({
      atributes: ['idtipofalta'],
      where: {
        idtipofalta,
        estaactivo: true,
      },
    })
    if (tipofalta.length > 0) {
      console.log(tipofalta.estaactivo)
      tipofalta.forEach(async tipoFalta => {
        await tipoFalta.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Tipo Falta eliminado.',
      })
    } else {
      return res.json({
        message:
          'Tipo Falta No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Tipo Falta.',
      data: {},
    })
  }
}

export async function habilitarTipoFalta(req, res) {
  const { idtipofalta } = req.params
  try {
    const tipofalta = await TipoFalta.findAll({
      atributes: ['idtipofalta'],
      where: {
        idtipofalta,
        estaactivo: false,
      },
    })
    if (tipofalta.length > 0) {
      console.log(tipofalta.estaactivo)
      tipofalta.forEach(async tipoFalta => {
        await tipoFalta.update({
          estaactivo: true,
        })
      })
      return res.json({
        message: 'Tipo Falta Habilitado.',
      })
    } else {
      return res.json({
        message:
          'Tipo Falta No encontrado, no existe el identificador o el estado esta activo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede habilitar el Tipo Falta.',
      data: {},
    })
  }
}
