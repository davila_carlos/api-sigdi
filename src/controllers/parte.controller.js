'use strict'

import Parte from '../models/Parte'
import Expediente from '../models/Expediente'
import Unidad from '../models/Unidad'
import TipoParte from '../models/TipoParte'
import Persona from '../models/Persona'
import Cargo from '../models/Cargo'

import { fechaDeHoy } from '../configfiles/datosExpediente'

export async function obtenerPartesExpediente(req, res) {
  //---

  const { idexpediente } = req.params

  try {
    if (idexpediente !== null) {
      //console.log(idexpediente);
      const existeexpediente = await Expediente.findAll({
        where: {
          idexpediente: idexpediente,
          estaactivo: true,
        },
      })

      if (existeexpediente.length > 0) {
        const partes = await Parte.findAll({
          where: {
            idexpediente,
            estaactivo: true,
          },
          include: [
            {
              model: Persona,
            },
            {
              model: TipoParte,
            },
            {
              model: Unidad,
            },
            {
              model: Cargo,
            },
          ],
        })

        if (partes.length > 0) {
          res.json({
            data: partes,
          })
        } else {
          res.status(500).json({
            message: 'El expediente no tiene partes registradas',
          })
        }
      } else {
        res.status(500).json({
          message:
            'El Identificador de expediente no esta registrado. ',
        })
      }
    } else {
      res.status(500).json({
        message: 'Debe indicar el identificador del expediente.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta',
    })
  }
}

export async function nuevaParte(req, res) {
  //---

  //console.log("nueva parte");
  //console.log(req.body);
  const { idexpediente, idunidad, idtipoparte, idpersona, idcargo } =
    req.body

  try {
    //---------verificando idexpediente----------------
    const existeexpediente = await Expediente.findAll({
      where: {
        idexpediente: idexpediente,
        estaactivo: true,
      },
    })
    //---------verificando idunidad-------------------
    const existeunidad = await Unidad.findAll({
      where: {
        idunidad: idunidad,
        estaactivo: true,
      },
    })
    //---------verificando idtipoparte----------------
    const existetipoparte = await TipoParte.findAll({
      where: {
        idtipoparte: idtipoparte,
        estaactivo: true,
      },
    })
    //---------verificando idpersona----------------
    const existepersona = await Persona.findAll({
      where: {
        idpersona: idpersona,
        estaactivo: true,
      },
    })
    //---------verificando idcargo----------------
    const existecargo = await Cargo.findAll({
      where: {
        idcargo: idcargo,
        estaactivo: true,
      },
    })

    if (
      existeexpediente.length > 0 &&
      existeunidad.length > 0 &&
      existetipoparte.length > 0 &&
      existepersona.length > 0 &&
      existecargo.length > 0
    ) {
      //--------Verificando Registro Anterior de Parte-----------------------------------------
      const verificarParteFueRegistrada = await Parte.findOne({
        where: {
          idexpediente,
          idpersona,
          estaactivo: true,
        },
      })

      if (verificarParteFueRegistrada === null) {
        let fechahoraregistro = fechaDeHoy
        let fechahoramodificado = fechaDeHoy

        let nuevaParte = await Parte.create(
          {
            idexpediente,
            idunidad,
            idtipoparte,
            idpersona,
            idcargo,
            fechahoraregistro,
            fechahoramodificado,
          },
          {
            fields: [
              'idexpediente',
              'idunidad',
              'idtipoparte',
              'idpersona',
              'idcargo',
              'fechahoraregistro',
              'fechahoramodificado',
            ],
          }
        )

        if (nuevaParte) {
          res.json({
            message:
              'Se registro a la parte en el expediente exitosamente.',
            data: nuevaParte,
          })
        } else {
          res.json({
            message: 'La parte no pudo ser registrada.',
            data: nuevaParte,
          })
        }
      } else {
        res.status(500).json({
          message:
            'La persona ya fue registrada como parte en el expediente anteriormente.',
        })
      }
    } else {
      let $mensajeError = '.'
      if (existeexpediente.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador del expediente no existe'
        console.log('El identificador del expediente no existe')
      }
      if (existeunidad.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador de la unidad no existe'
        console.log('El identificador de la unidad no existe')
      }

      if (existetipoparte.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador del tipo parte no existe'
        console.log('El identificador del tipo parte no existe')
      }

      if (existepersona.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador de la persona no existe'
        console.log('El identificador de la persona no existe')
      }

      if (existecargo.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador del cargo no existe'
        console.log('El identificador del cargo no existe')
      }

      res.status(500).json({
        message: `No se puede realizar el registro de la parte: ${$mensajeError}`,
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar la Parte',
      data: { e },
    })
  }
}

export async function obtenerParte(req, res) {
  //---

  const { idparte } = req.params

  try {
    const parte = await Parte.findOne({
      where: {
        idparte,
        estaactivo: true,
      },
      include: [
        {
          model: Persona,
        },
      ],
    })

    if (parte !== null) {
      res.json({
        data: parte,
      })
    } else {
      res.status(500).json({
        message:
          'Hubo un error, el identificador de la parte es incorrecto o el estado esta desabilitado.',
      })
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({
      data: { error },
      message: 'No se puedo procesar la consulta.',
    })
  }
}

//------------------------------------

export async function obtenerPartes(req, res) {
  try {
    const partes = await Parte.findAll({
      atributes: [
        'idparte',
        'idexpediente',
        'idunidad',
        'idtipoparte',
        'idpersona',
        'fechahoraregistro',
        'estaactivo',
        'fechahoramodificado',
      ],
      where: {
        estaactivo: true,
      },
    })

    res.json({
      data: partes,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function actualizarParte(req, res) {
  const { idparte } = req.params

  const { idexpediente, idunidad, idtipoparte, idpersona } = req.body

  try {
    if (idparte !== null) {
      const parte = await Parte.findAll({
        atributes: ['idparte'],
        where: {
          idparte,
          estaactivo: true,
        },
      })
      if (parte.length > 0) {
        let fechahoramodificado = new Date()
        parte.forEach(async parteactualizada => {
          await parteactualizada.update({
            idexpediente,
            idunidad,
            idtipoparte,
            idpersona,
            fechahoramodificado,
          })
        })
        return res.json({
          message: 'Parte actualizada con éxito',
          data: parte,
        })
      } else {
        return res.json({
          message:
            'Parte No encontrada, no existe el identificador o el estado esta inactivo ',
          data: parte,
        })
      }
    } else {
      res.status(500).json({
        message: 'Parte no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizarla parte',
      data: {},
    })
  }
}

export async function eliminaParte(req, res) {
  const { idparte } = req.params
  try {
    console.log(idparte)
    const parte = await Parte.findAll({
      atributes: ['idparte'],
      where: {
        idparte,
        estaactivo: true,
      },
    })
    if (parte.length > 0) {
      console.log('encontro un item')
      console.log(parte.estaactivo)
      parte.forEach(async parteactualizada => {
        await parteactualizada.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Parte eliminado.',
      })
    } else {
      return res.json({
        message:
          'Parte No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar a la parte.',
      data: {},
    })
  }
}
