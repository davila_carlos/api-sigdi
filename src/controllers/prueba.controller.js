'use strict'

import Prueba from '../models/Prueba'
import Expediente from '../models/Expediente'
import TipoPrueba from '../models/TipoPrueba'

import {
  rutaArchivoPrueba,
  nombreArchivoPrueba,
  fechaDeHoy,
} from '../configfiles/datosExpediente'

export async function obtenerPruebasExpediente(req, res) {
  //---

  const { idexpediente } = req.params

  try {
    if (idexpediente !== null) {
      const existeexpediente = await Expediente.findAll({
        where: {
          idexpediente: idexpediente,
          estaactivo: true,
        },
      })

      if (existeexpediente.length > 0) {
        const pruebas = await Prueba.findAll({
          where: {
            idexpediente: idexpediente,
            estaactivo: true,
          },
          include: [
            {
              model: TipoPrueba,
            },
          ],
          order: [['idprueba', 'DESC']],
        })

        if (pruebas.length > 0) {
          res.json({
            data: pruebas,
          })
        } else {
          res.status(500).json({
            message: 'El expediente no tiene pruebas registradas',
          })
        }
      } else {
        res.status(500).json({
          message:
            'El Identificador de expediente no esta registrado. ',
        })
      }
    } else {
      res.status(500).json({
        message: 'Debe indicar el identificador del expediente.',
      })
    }
  } catch (error) {
    console.log(error)
    res.json({
      data: { error },
      message: 'No se puedo procesar la consulta',
    })
  }
}

export async function nuevaPrueba(req, res) {
  //---

  //console.log("Ingresando a Controlador Prueba");
  //console.log(req.file);
  //console.log(req.body);

  const {
    idexpediente,
    idtipoprueba,
    numerofojas,
    ubicacionfisicadocumento,
    aceptada,
    descripcion,
  } = req.body

  try {
    const existeExpediente = await Expediente.findAll({
      where: {
        idexpediente: idexpediente,
        estaactivo: true,
      },
    })

    const existeTipoPrueba = await TipoPrueba.findAll({
      where: {
        idtipoprueba: idtipoprueba,
        estaactivo: true,
      },
    })

    if (existeExpediente.length > 0 && existeTipoPrueba.length > 0) {
      let fechahoraregistro = fechaDeHoy
      let fechahoramodificado = fechaDeHoy
      const ubicacionfisicadocumento =
        nombreArchivoPrueba + '_' + req.file.originalname

      //----------Verificando Registro Anterior de Prueba----------------------------------
      const verificarRegistroPrueba = await Prueba.findAll({
        where: {
          idexpediente,
          idtipoprueba,
          ubicacionfisicadocumento,
          aceptada: true,
        },
      })

      if (verificarRegistroPrueba.length === 0) {
        let nuevaPrueba = await Prueba.create(
          {
            idexpediente,
            idtipoprueba,
            numerofojas,
            ubicacionfisicadocumento,
            aceptada,
            //estaactivo,
            fechahoraregistro,
            fechahoramodificado,
            descripcion,
          },
          {
            fields: [
              'idexpediente',
              'idtipoprueba',
              'numerofojas',
              'ubicacionfisicadocumento',
              'aceptada',
              'fechahoraregistro',
              'fechahoramodificado',
              'descripcion',
            ],
          }
        )
        if (nuevaPrueba) {
          res.json({
            message: 'La Prueba fue registrada correctamente.',
            data: nuevaPrueba,
          })
        } else {
          res.status(500).json({
            message: 'La Prueba no pudo ser registrada.',
          })
        }
      } else {
        res.status(500).json({
          message: 'La Prueba ya fue registrada anteriormente.',
        })
      }
    } else {
      $mensajeError = '.'

      if (existeExpediente.length == 0) {
        $mensajeError =
          $mensajeError + 'El identificador del expediente no existe'
        console.log('El identificador del expediente no existe')
      }
      if (existeTipoPrueba.length == 0) {
        $mensajeError =
          $mensajeError +
          'El identificador del tipo de prueba no existe'
        console.log('El identificador del tipo de prueba no existe')
      }

      res.status(500).json({
        message: `No se puede realizar el registro de la prueba: ${$mensajeError}`,
      })
    }
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'No se puedo procesar la consulta',
      data: { e },
    })
  }
}

export async function obtenerArchivoPrueba(req, res) {
  //---

  try {
    //console.log(rutaArchivo);
    res.download(
      rutaArchivoPrueba + '/' + req.params.ubicacionfisicadocumento,
      req.params.ubicacionfisicadocumento
    )
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'No se pudo obtener el archivo.',
      data: { e },
    })
  }
}

//-------------------------------

export async function obtenerPrueba(req, res) {
  const { idprueba } = req.params
  try {
    const unaprueba = await Prueba.findOne({
      where: {
        idprueba,
      },
    })
    if (unaprueba !== null) {
      res.json(unaprueba)
    } else {
      res.status(500).json({
        message:
          'Hubo un error, no podemos obtener la prueba solicitada',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerPruebas(req, res) {
  try {
    const prueba = await Prueba.findAll({
      atributes: [
        'idprueba',
        'idexpediente',
        'idtipoprueba',
        'ubicacionfisicadocumento',
        'estaactivo',
        'fechahoraregistro',
        'numerofojas',
        'aceptada',
      ],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: prueba,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function eliminarPrueba(req, res) {
  const { idprueba } = req.params
  try {
    const prueba = await Prueba.findAll({
      atributes: ['idprueba'],
      where: {
        idprueba,
        estaactivo: true,
      },
    })
    if (prueba.length > 0) {
      console.log(prueba.estaactivo)
      prueba.forEach(async prueba => {
        await prueba.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'La prueba fue eliminada',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar la prueba',
      data: {},
    })
  }
}

export async function actualizarPrueba(req, res) {
  const { idprueba } = req.params
  const {
    idexpediente,
    idtipoprueba,
    ubicacionfisicadocumento,
    numerofojas,
    aceptada,
  } = req.body

  try {
    if (idprueba !== null) {
      const prueba = await Prueba.findAll({
        atributes: ['idprueba'],
        where: {
          idprueba,
          estaactivo: true,
        },
      })
      if (prueba.length > 0) {
        let fechahoramodificado = new Date()
        prueba.forEach(async pruebaactualizar => {
          await pruebaactualizar.update({
            idexpediente,
            idtipoprueba,
            ubicacionfisicadocumento,
            numerofojas,
            fechahoramodificado,
            aceptada,
          })
        })
        return res.json({
          message: 'Prueba actualizada con éxito',
          data: prueba,
        })
      } else {
        return res.json({
          message:
            'Prueba no encontrada, no existe el identificador o el estado esta inactivo ',
          data: prueba,
        })
        //res.status(500).json({ message: 'Prueba no encontrada' });
      }
    } else {
      res.status(500).json({
        message: 'Prueba no encontrada.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar la prueba',
      data: {},
    })
  }
}
