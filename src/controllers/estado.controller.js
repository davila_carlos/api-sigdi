'use strict'

import Estado from '../models/Estado'

export async function nuevoEstado(req, res) {
  const { descripcion } = req.body

  try {
    let nuevoEstado = await Estado.create(
      {
        descripcion,
      },
      {
        fields: ['descripcion'],
      }
    )
    if (nuevoEstado)
      return res.json({
        message: 'El Estado fue creado',
        data: nuevoEstado,
      })
  } catch (e) {
    console.log(e)
    res.status(500).json({
      message: 'Hubo un error, no se pudo registar el Estado',
      data: { e },
    })
  }
}

export async function obtenerEstado(req, res) {
  const { idestado } = req.params

  try {
    const estado = await Estado.findOne({
      where: {
        idestado,
      },
    })

    if (estado !== null) {
      res.json(estado)
    } else {
      res.status(500).json({
        message: 'Hubo un error, no se pudo encontrar el estado',
      })
    }
  } catch (error) {
    console.log(error)
  }
}

export async function obtenerEstados(req, res) {
  try {
    const estado = await Estado.findAll({
      atributes: ['idestado', 'descripcion', 'estaactivo'],
      where: {
        estaactivo: true,
      },
    })
    res.json({
      data: estado,
    })
  } catch (error) {
    console.log(error)
    res.json({
      data: {},
      message: 'Sucedió un error, lo sentimos',
    })
  }
}

export async function actualizarEstado(req, res) {
  const { idestado } = req.params

  const { descripcion } = req.body

  try {
    if (idestado !== null) {
      const estado = await Estado.findAll({
        //atributes: ['idtipodireccion', 'descripcion'],
        atributes: ['idestado'],
        where: {
          idestado,
          estaactivo: true,
        },
      })
      if (estado.length > 0) {
        estado.forEach(async estadoactualizada => {
          await estadoactualizada.update({
            descripcion,
          })
        })
        return res.json({
          message: 'Estado actualizado con éxito',
          data: estado,
        })
      } else {
        return res.json({
          message:
            'Estado No encontrado, no existe el identificador o el estado esta inactivo ',
          data: estado,
        })
      }
    } else {
      res.status(500).json({
        message: 'Estado no encontrado.',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede actualizar el estado',
      data: {},
    })
  }
}

export async function eliminaEstado(req, res) {
  const { idestado } = req.params
  try {
    const estado = await Estado.findAll({
      atributes: ['idestado'],
      where: {
        idestado,
        estaactivo: true,
      },
    })
    if (estado.length > 0) {
      console.log(estado.estaactivo)
      estado.forEach(async estadoactualizar => {
        await estadoactualizar.update({
          estaactivo: false,
        })
      })
      return res.json({
        message: 'Estado eliminado.',
      })
    } else {
      return res.json({
        message:
          'Estado No encontrado, no existe el identificador o el estado esta inactivo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede eliminar el Estado.',
      data: {},
    })
  }
}

export async function habilitarEstado(req, res) {
  const { idestado } = req.params
  try {
    const estado = await Estado.findAll({
      atributes: ['idestado'],
      where: {
        idestado,
        estaactivo: false,
      },
    })
    if (estado.length > 0) {
      console.log(estado.estaactivo)
      estado.forEach(async estadoactualizado => {
        await estadoactualizado.update({
          estaactivo: true,
        })
      })
      return res.json({
        message: 'Estado Habilitado.',
      })
    } else {
      return res.json({
        message:
          'Estado No encontrado, no existe el identificador o el estado esta activo',
      })
    }
  } catch (e) {
    res.json({
      message: 'No se puede habilitar el Estado.',
      data: {},
    })
  }
}
