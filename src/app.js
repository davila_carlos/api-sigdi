import { json } from 'body-parser'
import express from 'express'
import morgan from 'morgan'
import cors from 'cors'

//importing routes
import tipodireccionRoutes from './routes/tipodireccion'
import usuarioRoutes from './routes/usuario'
import rolRoutes from './routes/rol'
import rolExpedido from './routes/expedido'
//import expedidoRoutes  from './routes/expedido';
import unidadRoutes from './routes/unidad'
import tipopruebaRoutes from './routes/tipoprueba'
import sorteoRoutes from './routes/sorteo'
import plantillaRoutes from './routes/plantilla'
import instanciaRoutes from './routes/instancia'
import expedienteRoutes from './routes/expediente'
import pruebaRoutes from './routes/prueba'
import tipoactuadoRoutes from './routes/tipoactuado'
import actuadoRoutes from './routes/actuado'
import parteRoutes from './routes/parte'
import faltaparteRoutes from './routes/faltaparte'
import faltaRoutes from './routes/falta'
import personaRoutes from './routes/persona'
import estadoRoutes from './routes/estado'
import tipofaltaRoutes from './routes/tipofalta'
import tipoparteRoutes from './routes/tipoparte'
import expedidoRoutes from './routes/expedido'
import direccionRoutes from './routes/direccion'
import cargoRoutes from './routes/cargo'
import tipomedidaprecautoriaRoutes from './routes/tipomedidaprecautoria'

//initialization
const app = express()

//middelwares
app.use(morgan('dev'))
app.use(json())
app.use(cors())

// routes
app.use('/api/tipodireccion', tipodireccionRoutes)
app.use('/api/usuario', usuarioRoutes)
app.use('/api/rol', rolRoutes)
app.use('/api/expedido', rolExpedido)
//app.use('/api/expedido',expedidoRoutes);
app.use('/api/unidad', unidadRoutes)
app.use('/api/tipoprueba', tipopruebaRoutes)
app.use('/api/sorteo', sorteoRoutes)
app.use('/api/plantilla', plantillaRoutes)
app.use('/api/instancia', instanciaRoutes)
app.use('/api/expediente', expedienteRoutes)
app.use('/api/prueba', pruebaRoutes)
app.use('/api/tipoactuado', tipoactuadoRoutes)
app.use('/api/actuado', actuadoRoutes)
app.use('/api/parte', parteRoutes)
app.use('/api/faltaparte', faltaparteRoutes)
app.use('/api/falta', faltaRoutes)
app.use('/api/persona', personaRoutes)
app.use('/api/estado', estadoRoutes)
app.use('/api/tipofalta', tipofaltaRoutes)
app.use('/api/tipoparte', tipoparteRoutes)
app.use('/api/expedido', expedidoRoutes)
app.use('/api/direccion', direccionRoutes)
app.use('/api/cargo', cargoRoutes)
app.use('/api/tipomedidaprecautoria', tipomedidaprecautoriaRoutes)

export default app
