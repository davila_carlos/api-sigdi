//import app from './app';
import multer from 'multer'
import path from 'path'

export const fechaDeHoy = new Date()
//console.log("FECHA DE HOY");
//console.log(fechaDeHoy.getHours());

//----------------------------Archivo Expediente--------------------------------------------------------------
export const nombreArchivo = `RH-${fechaDeHoy.getFullYear()}${
  fechaDeHoy.getMonth() + 1
}${fechaDeHoy.getDate()}`
export const rutaArchivo = path.join(
  __dirname,
  '../files/expedientes'
)

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, rutaArchivo)
  },
  filename: function (req, file, cb) {
    //cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    //cb(null,`${file.originalname}_${fecha.getFullYear()}${fecha.getMonth()+1}${fecha.getDate()}.${file.mimetype.split('/')[1]}`);
    //console.log(`${fechaDeHoy.getFullYear()}${fechaDeHoy.getMonth()+1}${fechaDeHoy.getDate()}_${file.originalname}`);
    cb(null, `${nombreArchivo}_${file.originalname}`)
    //cb(null,file);
    //console.log(nombrefile);
  },
})
export const uploadExpediente = multer({ storage })

//----------------------------Archivo de Prueba-----------------------------------------------------------------------

export const nombreArchivoPrueba = `PB-${fechaDeHoy.getFullYear()}${
  fechaDeHoy.getMonth() + 1
}${fechaDeHoy.getDate()}`
export const rutaArchivoPrueba = path.join(
  __dirname,
  '../files/prueba'
)

storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, rutaArchivoPrueba)
  },
  filename: function (req, file, cb) {
    cb(null, `${nombreArchivoPrueba}_${file.originalname}`)
  },
})

export const uploadPrueba = multer({ storage })
