import app from './app'
import multer from 'multer'
import path from 'path'
//const path = require('path');
//const multer = require('multer')

//configurando multer
/*const confmulter = multer.diskStorage({
        destination: path.join(__dirname,'/public/archivos'),
        filename: (req, relatohechos, cb)=>{
            cb(null, relatohechos.originalname);
        }
    });
    
    app.use(multer({
        storage:confmulter,
        dest: path.join(__dirname,'public/uploads')
    }).single('image'));*/

const fecha = new Date()
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '/files/expedientes'))
  },
  filename: function (req, file, cb) {
    //cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    //cb(null,`${file.originalname}_${fecha.getFullYear()}${fecha.getMonth()+1}${fecha.getDate()}.${file.mimetype.split('/')[1]}`);
    cb(
      null,
      `${fecha.getFullYear()}${
        fecha.getMonth() + 1
      }${fecha.getDate()}_${file.originalname}`
    )
    //console.log(Date.now());
    //cb(null,file);
  },
})

//middlewares
/*const subirarchivo = multer({
    storage:confmulter,
    dest: path.join(__dirname,'public/uploads')
}).single('image');*/

const upload = multer({ storage })

//const upload = multer({dest:'images/'});

/*const upload = multer({
    destination:function(req,file, cb){
        cb(null,path.join(__dirname,'/public/uploads'));
    },
    filename:function(req, file, cb){
        cb(null,`${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
        //cb(null,file);
    }
})*/

export default upload
