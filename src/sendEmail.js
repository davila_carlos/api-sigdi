require('dotenv').config()
const nodemailer = require('nodemailer')

let userEmail = process.env.EMAIL
let userPass = process.env.PASSWORD

async function sendEmail(to, subject, name, token) {
  let transporter = nodemailer.createTransport({
    host: process.env.MAIL_SERVER || 'smtp.googlemail.com',
    port: process.env.MAIL_PORT || 587,
    secure: false,
    auth: {
      user: userEmail,
      pass: userPass,
    },
  })

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: 'Consejo de la magistratura <consejo@magistratura.com>',
    to: to,
    subject: subject,
    text: `Estimado ${name}
    Bienvenido a SIGDI!
    Para confirmar tu cuenta por favor accede al siguiente enlace:
    localhost:3000/api/usuario/confirm/${token}
    
    Sinceramente,
    El equipo de SIGDI
    Nota: Las respuestas a esta dirección de correo electrónico no se controlan.
    `,

    html: `
    <p>Estimado ${name},</p>
    <p>Bienvenido a <b>SIGDI</b>!</p>
    <p>Para confirmar tu cuenta por favor accede al siguiente enlace <a href="http://localhost:3000/api/usuario/confirm/${token}">click here</a>.</p>
    <p>O puedes copiar y pegar el siguiente enlace</p>
    <p>http://localhost:3000/api/usuario/confirm/${token}</p>
    
    <p>Sinceramente,</p>
    <p>El equipo de SIGDI</p>
    <p><small>Nota: Las respuestas a esta dirección de correo electrónico no se controlan.</small></p>`,
  })

  console.log(`Mensaje enviado: ${info.messageId}`)
}

export default sendEmail
